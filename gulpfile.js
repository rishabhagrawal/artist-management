var gulp = require('gulp'),
    concat = require('gulp-concat'),
    minifyCSS = require('gulp-cssnano'),
    notify = require('gulp-notify'),
    sass = require('gulp-sass'),
    plumber = require('gulp-plumber'),
    livereload = require('gulp-livereload'),
    uglify = require('gulp-uglify'),
    jsValidate = require('gulp-jsvalidate'),
    htmlmin = require('gulp-htmlmin'),
    phpMinify = require('gulp-php-minify');

var client = {
    'php': ['./develop/pages/client/**/**/**/**/**/**/*.php'],
    'vendorJs': ['./node_modules/angular/angular.min.js',
        './node_modules/angular-sanitize/angular-sanitize.min.js',
        './node_modules/angucomplete-alt/dist/angucomplete-alt.min.js',
    ],
    'vendorCss': [
        './node_modules/angular/angular-csp.css',
        './assets/icons/style.css'
    ],
    'appJs': [
        './develop/scripts/app.globalConfig.js',
        './develop/scripts/app.module.js',
        './develop/scripts/constants/*.js',
        './develop/scripts/filters/*.js',
        './develop/scripts/directives/**/*.js',
        './develop/scripts/services/*.js',
        './develop/pages/client/**/**/**/**/**/*.js'
    ],
    'appCss': [
        './develop/styles/**/**/**/**/*.scss',
        './develop/pages/client/**/**/**/**/**/**/**/*.scss'
    ]
};

var jsPath = {
    'vendor': ['./node_modules/jquery/dist/jquery.min.js',
        './node_modules/angular/angular.min.js',
        './node_modules/angular-sanitize/angular-sanitize.min.js',
        './node_modules/angucomplete-alt/dist/angucomplete-alt.min.js',
    ],
    'clientVendor': ['./node_modules/angular/angular.min.js',
        './node_modules/angular-sanitize/angular-sanitize.min.js',
        './node_modules/angucomplete-alt/dist/angucomplete-alt.min.js',
    ],
    'adminApp': [
        './develop/scripts/app.globalConfig.js',
        './develop/scripts/app.module.js',
        './develop/scripts/constants/*.js',
        './develop/scripts/filters/*.js',
        './develop/scripts/directives/**/*.js',
        './develop/scripts/services/*.js',
        './develop/pages/admin/**/**/**/**/**/*.js'
    ]
};

var config = {
    'php': [
        './develop/pages/**/**/**/**/**/**/**/*.php'
    ],
    'vendorCss': [
        './node_modules/angular/angular-csp.css',
        './node_modules/angucomplete-alt/angucomplete-alt.css',
        './assets/icons/style.css'
    ],
    'adminSass': [
        './develop/styles/**/**/**/**/*.scss',
        './develop/pages/admin/**/**/**/**/**/**/**/*.scss'
    ]
}

// ******* PHP ********
// move php files and minified file.
gulp.task('phpFile', function() {
    gulp.src(config.php)
        .pipe(phpMinify())
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('./application/views/'));
});

gulp.task('ClientPhpFile', function() {
    gulp.src(client.php)
        .pipe(phpMinify())
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('./application/views/client/'));
});


//=============
// Vendor CSS
//=============
//vendor css compile and minified
gulp.task('vendor-css', function() {
    gulp.src(config.vendorCss)
        .pipe(minifyCSS())
        .pipe(concat('vendor.min.css'))
        .pipe(gulp.dest('./assets/css/'));
});

gulp.task('client-vendor-css', function() {
    gulp.src(client.vendorCss)
        .pipe(minifyCSS())
        .pipe(concat('client.vendor.min.css'))
        .pipe(gulp.dest('./assets/css/'));
});

//=============
// App CSS
//=============
gulp.task('client-scss', function() {
    gulp.src('develop/pages/client/style.scss')
        .pipe(plumber({
            errLogToConsole: true,
            errorHandler: notify.onError({
                "title": "Sass Error",
                "message": "Sass Error"
            })
        }))
        .pipe(sass())
        .on("error", console.log)
        .pipe(minifyCSS())
        .pipe(concat('client.app.min.css'))
        .pipe(gulp.dest('./assets/css/'))
});

gulp.task('admin-scss', function() {
    gulp.src('develop/pages/admin/style.scss')
        .pipe(plumber({
            errLogToConsole: true,
            errorHandler: notify.onError({
                "title": "Sass Error",
                "message": "Sass Error"
            })
        }))
        .pipe(sass())
        .on("error", console.log)
        .pipe(minifyCSS())
        .pipe(concat('admin.app.min.css'))
        .pipe(gulp.dest('./assets/css/'))
        .pipe(livereload());
});

//=============
//Vendor JS
//=============
//vendor javascript compile and minified
gulp.task('vendor-js', function() {
    gulp.src(jsPath.vendor)
        .pipe(uglify())
        .pipe(concat('vendor.min.js'))
        .pipe(gulp.dest('./assets/js/'));
});

gulp.task('client-vendor-js', function() {
    gulp.src(client.vendorJs)
        .pipe(uglify())
        .pipe(concat('client.vendor.min.js'))
        .pipe(gulp.dest('./assets/js/'));
});


//=============
//App JS
//=============
// app javascript for Client compile and concat.
gulp.task('client-js', function() {
    gulp.src(client.appJs)
        .pipe(plumber({
            errLogToConsole: true,
            errorHandler: notify.onError({
                "title": "Javascript Error",
                "message": "Javascript Error"
            })
        }))
        .pipe(concat('client.app.min.js'))
        .pipe(jsValidate())
        .on("error", console.log)
        // .pipe(uglify())
        .pipe(gulp.dest('./assets/js/'))
        .pipe(livereload());
});

// app javascript for Admin compile and concat.
gulp.task('admin-js', function() {
    gulp.src(jsPath.adminApp)
        .pipe(plumber({
            errLogToConsole: true,
            errorHandler: notify.onError({
                "title": "Javascript Error",
                "message": "Javascript Error"
            })
        }))
        .pipe(concat('admin.app.min.js'))
        .pipe(jsValidate())
        .on("error", console.log)
        // .pipe(uglify())
        .pipe(gulp.dest('./assets/js/'))
        .pipe(livereload());
});



//=============
// WEB WATCH
//=============
gulp.task('watch', function() {
    livereload.listen();
    gulp.watch(config.php, ['phpFile']); //watch php files 
    gulp.watch(config.clientSass, ['client-scss']); // watch and reload client Sass
    gulp.watch(config.adminSass, ['admin-scss']); // watch and reload admin Sass
    gulp.watch(jsPath.clientApp, ['client-js']); // watch and reload client Javascript
    gulp.watch(jsPath.adminApp, ['admin-js']); // watch and reload admin Javascript
});

gulp.task('client-watch', function() {
    livereload.listen();
    gulp.watch(client.php, ['ClientPhpFile']); //watch php files 
    gulp.watch(client.appJs, ['client-js']); // watch and reload client Sass
    gulp.watch(client.appCss, ['client-scss']); // watch and reload admin Sass
});

gulp.task('client', [
    'ClientPhpFile',
    'client-vendor-css',
    'client-scss',
    'client-vendor-js',
    'client-js',
    'client-watch'
]);

gulp.task('default', [
    'phpFile',
    'vendor-css',
    'client-scss',
    'admin-scss',
    'vendor-js',
    'client-vendor-js',
    'client-js',
    'admin-js',
    'watch'
]);