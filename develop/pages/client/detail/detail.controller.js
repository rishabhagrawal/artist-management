(function() {

    'use strict';

    angular.module(globalConfig.appName).controller('DetailController', [
        '$scope',
        'DetailFactory',
        '$timeout',
        'HomeFactory',
        function(
            $scope,
            DetailFactory,
            $timeout,
            HomeFactory
        ) {

            var currentUrl = window.location.href;
            var splitUrl = currentUrl.split('-');
            var artist_id = splitUrl[splitUrl.length - 1];

            $scope.labelToSlug = function(string) {
                var data = string.toLowerCase();
                return data.split(' ').join('-');
            }

            DetailFactory.getDetail(artist_id).then(function(result) {
                $scope.artist_data = result.data.artist_data;
            }, function(error) {});

            var getListing = function() {
                $scope.isLoading = true;
                HomeFactory.allFeatureListing().then(function(result) {
                    $scope.featureArtists = result.data.artists;
                    $scope.isLoading = false;
                }, function(error) {})
            }
            getListing();

        }
    ]);

})();