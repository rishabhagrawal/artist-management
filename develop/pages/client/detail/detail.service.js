(function() {
    'use strict';

    angular.module(globalConfig.appName).factory('DetailFactory', [
        '$http',
        '$q',
        function(
            $http,
            $q
        ) {
            var baseurl = window.baseUrl;
            
            return {
                getDetail: getDetail
            }

            function getDetail(artist_id) {
                var deferred = $q.defer();
                var req = {
                    method: "GET",
                    url: baseurl + 'api/artist-detail/'+artist_id,
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }
        }
    ]);

})();