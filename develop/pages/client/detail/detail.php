<section id="artist-detail" ng-controller="DetailController">
	<div class="artist-wrapper clearfix">
		<div class="left-section">
			<div class="artist-image">
				<img src="{{artist_data.image}}" alt="">
			</div>
		</div>
		<div class="right-section">
			<div class="artist-name">
				{{artist_data.name}}
				<span>({{artist_data.category}})</span>
			</div>
			<div class="artist-description">
				{{artist_data.bio}}
			</div>
			<ul class="location-ul clearfix">
				<li>
					<img src="<?php echo base_url('assets/images/location.png'); ?>" alt="">
					{{artist_data.location}}
				</li>
				<!-- <li>
					<img src="<?php echo base_url('assets/images/phone.png'); ?>" alt="">
					+91-8010979311
				</li> -->
			</ul>
			<div class="book-btn">
				<a href="<?php echo base_url('requirement') ?>">
					<button>BOOK MY ARTIST</button>
				</a>
			</div>
		</div>
	</div>
	<div class="featured-artist-section">
		<h2>FEATURED <span>ARTISTS</span></h2>
		<ul class="artist-list-ul clearfix">
			<li ng-repeat="data in featureArtists" ng-show="$index < 4">
				<a href="<?php echo base_url() ?>artist-detail/{{labelToSlug(data.url)}}-{{data.id}}" class="artist-info">
					<div class="artist-image">
						<img src="{{data.image}}" alt="artist image" ng-show="data.image">
						<img src="http://diymusician.cdbaby.com/wp-content/uploads/2014/06/shutterstock_152104547.jpg" alt="artist image" ng-show="!data.image" style="height:94%;">
					</div>
					<div class="artist-name">{{data.name}}</div>
					<div class="artist-profile">{{data.category}}</div>
				</a>
			</li>
		</ul>
	</div>
</section>