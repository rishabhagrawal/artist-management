(function() {

    'use strict';

    angular.module(globalConfig.appName).controller('homeController', [
        '$scope',
        'HomeFactory',
        function($scope,
            HomeFactory
        ) {

            HomeFactory.artistsCategories().then(function(result) {
                $scope.jsonFormat = result.data.result;
            }, function(error) {})

            $scope.labelToSlug = function(string) {
                var data = string.toLowerCase();
                return data.split(' ').join('-');
            }

            $scope.selectedArtist = function(data) {
                if (data.originalObject && data.originalObject.type) {
                    var baseUrlArray = window.baseUrl.split('/');
                    if (baseUrlArray[baseUrlArray.length - 1] == '') {
                        var baseUrl = window.baseUrl;
                    } else {
                        var baseUrl = window.baseUrl + '/';
                    }
                    if (data.originalObject.type == 'category') {
                        window.location.href = window.baseUrl + "listing/" + $scope.labelToSlug(data.originalObject.url);
                    }
                    if (data.originalObject.type == 'artist') {
                        window.location.href = window.baseUrl + "artist-detail/" + encodeURI($scope.labelToSlug(data.originalObject.url)) + "-" + data.originalObject.id;
                    }
                }
            }

            var getListing = function() {
                $scope.isLoading = true;
                HomeFactory.allFeatureListing().then(function(result) {
                    $scope.featureArtists = result.data.artists;
                    $scope.isLoading = false;
                }, function(error) {})
            }
            getListing();
        }
    ]);

})();