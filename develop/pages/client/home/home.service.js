(function() {
    'use strict';

    angular.module(globalConfig.appName).factory('HomeFactory', [
        '$http',
        '$q',
        function(
            $http,
            $q
        ) {
            var baseurl = window.baseUrl;
            
            return {
                allFeatureListing: allFeatureListing,
                artistsCategories : artistsCategories
            }

            function allFeatureListing(data) {
                var deferred = $q.defer();
                var req = {
                    method: "GET",
                    url: baseurl + 'api/featured-artists',
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }
            function artistsCategories(data) {
                var deferred = $q.defer();
                var req = {
                    method: "GET",
                    url: baseurl + 'api/artists-categories',
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }
        }
    ]);

})();