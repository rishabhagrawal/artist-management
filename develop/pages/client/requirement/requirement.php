<section id="requirement" ng-controller="requirementController">
	<div class="form-section">
		<div>Kindly mention your name: <input type="text" class="name-text" placeholder="Name" ng-model="formData.name" ng-class="{'error' : nameError}"></div>
		<div>I am looking for a <select ng-change="categoryChoose()" ng-model="selectCategory" ng-options="data as data.name for data in categories track by data.id" ng-class="{'error' : categoryError}" class="category-text"></select> in <select ng-change="locationChoose()" ng-model="selectLocation" ng-options="data as data.name for data in locations track by data.id" ng-class="{'error' : locationError}" class="location-text"></select> for</div>
		<div><input type="text" placeholder="Event Type" class="event-text" ng-model="formData.event_type"> on <input type="date" placeholder="Date" class="date-text" ng-model="formData.event_date"> with approximately <input type="number" min="0" placeholder="n" class="number-text" ng-model="formData.no_of_guests"> number</div>
		<div>of guest. Contact me at <input type="text" placeholder="Phone Number" ng-model="formData.contact_no" ng-class="{'error' : contactError}" class="mobile-text" maxlength="10">, <input type="text" placeholder="Email" class="email-text" ng-model="formData.email" ng-class="{'error' : emailError}">.</div>
		<div class="submit-btn">
			<div class="success-msg" ng-show="successMsg">{{successMsg}}</div>
			<button ng-click="registerBtn()">SUBMIT</button>
		</div>
	</div>
</section>