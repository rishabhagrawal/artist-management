(function() {

    'use strict';

    angular.module(globalConfig.appName).controller('requirementController', [
        '$scope',
        'LocationFactory',
        'CategoryFactory',
        'RequirementFactory',
        '$timeout',
        function(
            $scope,
            LocationFactory,
            CategoryFactory,
            RequirementFactory,
            $timeout
        ) {

            function valueInit() {
                $scope.formData = {
                    name: "",
                    category: "", // id
                    location: "", // id
                    event_type: "", //textarea
                    event_date: "", //date
                    no_of_guests : "",
                    contact_no: "",
                    email: "",
                }
            }
            valueInit();

            function validateEmail(email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }

            LocationFactory.getAllCities().then(function(result) {
                $scope.locations = result.data.locations;
                var locationFirstObj = { id: "", name: "Select Location" }
                $scope.locations.unshift(locationFirstObj);
                $scope.selectLocation = $scope.locations[0];
            }, function(error) {});

            CategoryFactory.getAllCategory().then(function(result) {
                $scope.categories = result.data.categories;
                var categoryFirstObj = { id: "", name: "Select Category" }
                $scope.categories.unshift(categoryFirstObj);
                $scope.selectCategory = $scope.categories[0];
            }, function(error) {});

            $scope.categoryChoose = function() {
                $scope.formData.category = $scope.selectCategory.id;
            }
            $scope.locationChoose = function() {
                $scope.formData.location = $scope.selectLocation.id;
            }

            $scope.registerBtn = function() {
                var isValid = true;
                if (!$scope.formData.name) {
                    isValid = false;
                    $scope.nameError = true;
                }
                if (!$scope.formData.category) {
                    isValid = false;
                    $scope.categoryError = true;
                }
                if (!$scope.formData.location) {
                    isValid = false;
                    $scope.locationError = true;
                }
                if (!$scope.formData.contact_no) {
                    isValid = false;
                    $scope.contactError = true;
                }
                if ($scope.formData.email) {
                    if (!validateEmail($scope.formData.email)) {
                        $scope.emailError = true;
                        isValid = false;
                    }
                }
                if (isValid) {
                    RequirementFactory.formSubmit($scope.formData).then(function(result){
                    },function(error){
                    })
                    valueInit();
                    $scope.successMsg = "Your Details has been successfully Submitted."
                    $scope.selectLocation = $scope.locations[0];
                    $scope.selectCategory = $scope.categories[0];
                    $timeout(function() {
                        $scope.successMsg = ''
                    }, 4000)
                    
                }
            }

            $scope.$watch('formData', function(newVal, oldVal) {
                if (newVal !== oldVal) {
                    $scope.nameError = false;
                    $scope.categoryError = false;
                    $scope.locationError = false;
                    $scope.contactError = false;
                    $scope.emailError = false;
                }
            }, true)

        }
    ]);

})();