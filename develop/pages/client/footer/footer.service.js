(function() {
    'use strict';

    angular.module(globalConfig.appName).factory('FooterFactory', [
        '$http',
        '$q',
        function(
            $http,
            $q
        ) {
            var baseurl = window.baseUrl;
            
            return {
                formSubmit: formSubmit
            }

            function formSubmit(data) {
                var deferred = $q.defer();
                var req = {
                    method: "POST",
                    url: baseurl + 'api/contact-us',
                    data : data
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }
        }
    ]);

})();