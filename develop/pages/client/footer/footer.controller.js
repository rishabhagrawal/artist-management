(function() {

    'use strict';

    angular.module(globalConfig.appName).controller('footerController', [
        '$scope',
        'FooterFactory',
        '$timeout',
        function(
            $scope,
            FooterFactory,
            $timeout
        ) {

            function valueInit() {
                $scope.formData = {
                    name: "",
                    contact_no: "",
                    email: "",
                    subject: "",
                    message: "", //textarea
                }
            }
            valueInit();

            function validateEmail(email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }

            $scope.submitBtn = function() {
                var isValid = true;
                if (!$scope.formData.name) {
                    isValid = false;
                    $scope.nameError = true;
                }
                if (!$scope.formData.contact_no) {
                    isValid = false;
                    $scope.contactError = true;
                }
                if ($scope.formData.email) {
                    if (!validateEmail($scope.formData.email)) {
                        $scope.emailError = true;
                        isValid = false;
                    }
                }
                if (isValid) {
                    FooterFactory.formSubmit($scope.formData).then(function(result) {

                    }, function(error) {
                    });
                    console.log($scope.formData);
                    $scope.successMsg = "Your message has been successfully send."
                    $timeout(function() {
                        $scope.successMsg = ''
                    }, 4000);
                    valueInit();
                }
            }

            $scope.$watch('formData', function(newVal, oldVal) {
                if (newVal !== oldVal) {
                    $scope.nameError = false;
                    $scope.contactError = false;
                    $scope.emailError = false;
                }
            }, true)

        }
    ]);

})();