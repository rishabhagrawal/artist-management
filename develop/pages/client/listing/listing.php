<section id="listing" ng-controller="listingController">
	<div class="top-bar clearfix">
		<h4 ng-show="totalCount > 0">{{pageTitle}} <span>(showing {{ (count*(currentPage - 1)) + 1 }} to {{ count*currentPage}}  of {{totalCount}} artists)</span></h4>
		<ul class="filter-ul clearfix">
			<li>
				<span>Category: </span>
				<select ng-change="categoryChoose()" ng-model="selectCategory" ng-options="data as data.name for data in categories track by data.id">
					<!-- <option value="">Choose Category</option>
					<option value="{{data.id}}" ng-repeat="data in categories track by data.id">{{data.name}}</option> -->
				</select>
				<div class="remove-icon" ng-show="selectCategory" ng-click="deleteFilter('category')">
					<i class="icon-cross"></i>
				</div>
			</li>
			<li>
				<span>City: </span>
				<select ng-change="locationChange(selectLocation)" ng-model="selectLocation">
					<option value="">Choose City</option>
					<option value="{{data.id}}" ng-repeat="data in cities">{{data.name}}</option>
				</select>
				<div class="remove-icon" ng-show="selectLocation" ng-click="deleteFilter('location')">
					<i class="icon-cross"></i>
				</div>
			</li>
			<!-- <li>
				<span>Sort by: </span>
				<select name="" id="">
					<option value="">A - Z</option>
					<option value="">Z - A</option>
				</select>
			</li> -->
		</ul>
	</div>
	<ul class="artist-list-ul clearfix">
		<li ng-repeat="data in artists">
			<a href="<?php echo base_url();?>artist-detail/{{labelToSlug(data.url)}}-{{data.id}}" class="artist-info">
				<div class="artist-image">
					<img src="{{data.image}}" alt="artist image" ng-show="data.image">
					<img src="http://diymusician.cdbaby.com/wp-content/uploads/2014/06/shutterstock_152104547.jpg" alt="artist image" ng-show="!data.image" style="height:94%;">
				</div>
				<div class="artist-name">{{data.name}}</div>
				<div class="artist-profile">{{data.category}}</div>
			</a>
		</li>
	</ul>
	<div class="no-artist-section" ng-show="totalCount == 0">
		No Artist Found!
	</div>
	<div class="paggination" ng-show="totalCount > 0">
		<ul>
			<li ng-click="prevPagination()" ng-class="{'disable':(currentPage == 1)}">
				<i class="icon-left-arrow"></i>
			</li>
			<li ng-class="{'active': currentPage == pagiOne}" ng-show="pagiOne" ng-click="gotoPage(pagiOne)">
				{{pagiOne}}
			</li>
			<li ng-class="{'active': currentPage == pagiTwo}" ng-show="pagiTwo" ng-click="gotoPage(pagiTwo)">
				{{pagiTwo}}
			</li>
			<li ng-show="isDotShow">
				...
			</li>
			<li ng-class="{'active': currentPage == pagiSecond}" ng-show="pagiSecond" ng-click="gotoPage(pagiSecond)">
				{{pagiSecond}}
			</li>
			<li ng-class="{'active': currentPage == pagiLast}" ng-show="pagiLast" ng-click="gotoPage(pagiLast)">
				{{pagiLast}}
			</li>
			<li ng-click="nextPagination()" ng-class="{'disable':(currentPage == totalPages)}">
				<i class="icon-right-arrow"></i>
			</li>
		</ul>
	</div>
</section>