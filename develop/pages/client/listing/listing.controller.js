(function() {
    'use strict';
    angular.module(globalConfig.appName).controller('listingController', [
        '$scope',
        'ListingFactory',
        'LocationFactory',
        'CategoryFactory',
        function(
            $scope,
            ListingFactory,
            LocationFactory,
            CategoryFactory
        ) {

            $scope.count = 12;
            $scope.currentPage = 1;
            var applied_filter = {}

            var listingUrl = window.baseUrl+"listing/";
            var currentUrl = window.location.href;
            var categoryName = currentUrl.replace(listingUrl,'');
            $scope.pageTitle = "CELEBRITIES"
            if(categoryName){
                if(categoryName == 'singer'){
                    $scope.pageTitle = "SINGERS";
                }
                if(categoryName == 'dancer'){
                    $scope.pageTitle = "DANCERS";
                }
                if(categoryName == 'anchor'){
                    $scope.pageTitle = "ANCHORS";
                }
                if(categoryName == 'dj'){
                    $scope.pageTitle = "DJ's";
                }
                if(categoryName == 'band'){
                    $scope.pageTitle = "BANDS";
                }
                if(categoryName == 'stand-up-comedy'){
                    $scope.pageTitle = "STAND-UP COMEDIANS";
                }
                if(categoryName == 'celebrity'){
                    $scope.pageTitle = "CELEBRITIES";
                }
                if(categoryName == 'influence'){
                    $scope.pageTitle = "INFLUENCE";
                }
            }

            LocationFactory.getAllCities().then(function(result) {
                $scope.cities = result.data.locations;
            }, function(error) {});

            $scope.labelToSlug = function(string) {
                var data = string.toLowerCase();
                return data.split(' ').join('-');
            }

            CategoryFactory.getAllCategory().then(function(result) {
                $scope.categories = result.data.categories;
                if (categoryName) {
                    angular.forEach($scope.categories, function(val, key) {
                        if ($scope.labelToSlug(val.name) == categoryName) {
                            applied_filter.category = val.id;
                            $scope.selectCategory = val;
                            getListing();
                        }
                    })
                }
            }, function(error) {});

            var getListing = function() {
                applied_filter.page = $scope.currentPage;
                applied_filter.count = $scope.count;
                $scope.isLoading = true;
                ListingFactory.allListing(applied_filter).then(function(result) {
                    $scope.artists = result.data.artists;
                    $scope.totalCount = result.data.total;
                    $scope.isLoading = false;
                    $scope.totalPages = Math.ceil(result.data.total / $scope.count);
                    if ($scope.totalPages > 5) {
                        if ($scope.totalPages - 3 <= $scope.currentPage) {
                            if (($scope.totalPages - 3) <= 0) {
                                $scope.pagiOne = undefined;
                            } else {
                                $scope.pagiOne = $scope.totalPages - 3;
                            }
                            if (($scope.totalPages - 2) <= 0) {
                                $scope.pagiTwo = undefined
                            } else {
                                $scope.pagiTwo = $scope.totalPages - 2;
                            }
                            if (($scope.totalPages - 1) <= 0) {
                                $scope.pagiSecond = undefined;
                            } else {
                                $scope.pagiSecond = $scope.totalPages - 1;
                            }
                            $scope.pagiLast = $scope.totalPages;
                            $scope.isDotShow = false;
                        } else {
                            $scope.pagiOne = $scope.currentPage;
                            $scope.pagiTwo = $scope.currentPage + 1;
                            $scope.pagiSecond = $scope.totalPages - 1;
                            $scope.pagiLast = $scope.totalPages;
                            $scope.isDotShow = true;
                        }
                    } else {
                        if (($scope.totalPages - 3) <= 0) {
                            $scope.pagiOne = undefined;
                        } else {
                            $scope.pagiOne = $scope.totalPages - 3;
                        }
                        if (($scope.totalPages - 2) <= 0) {
                            $scope.pagiTwo = undefined
                        } else {
                            $scope.pagiTwo = $scope.totalPages - 2;
                        }
                        if (($scope.totalPages - 1) <= 0) {
                            $scope.pagiSecond = undefined;
                        } else {
                            $scope.pagiSecond = $scope.totalPages - 1;
                        }
                        $scope.isDotShow = false;
                        $scope.pagiLast = $scope.totalPages;
                    }
                    // $scope.paginationOne = ;
                }, function(error) {

                })
            }
            getListing();

            $scope.prevPagination = function() {
                $scope.currentPage = $scope.currentPage - 1;
                getListing();
            }

            $scope.nextPagination = function() {
                $scope.currentPage = $scope.currentPage + 1;
                getListing();
            }

            $scope.locationChange = function(data) {
                $scope.count = 12;
                $scope.currentPage = 1;
                if (data != '') {
                    applied_filter.location = data;
                } else {
                    delete applied_filter.location;
                }
                getListing()
            }

            $scope.categoryChoose = function(){
                $scope.count = 12;
                $scope.currentPage = 1;
                if ($scope.selectCategory.id != '') {
                    var currentUrl = window.location.href;
                    var currentUrl = window.baseUrl + "listing/" +  $scope.labelToSlug($scope.selectCategory.name);
                    window.history.replaceState(null, null, currentUrl);
                    applied_filter.category = $scope.selectCategory.id;
                } else {
                    var currentUrl = window.location.href;
                    var currentUrl = window.baseUrl + "listing";
                    window.history.replaceState(null, null, currentUrl);
                    delete applied_filter.category;
                }
                getListing()
            }

            $scope.deleteFilter = function(data){
                $scope.count = 12;
                $scope.currentPage = 1;
                delete applied_filter[data];
                if(data == 'category'){
                    $scope.selectCategory = false;
                    var currentUrl = window.location.href;
                    var currentUrl = window.baseUrl + "listing";
                    window.history.replaceState(null, null, currentUrl);
                }
                if(data == 'location'){
                    $scope.selectLocation = false;
                }
                getListing();
            }

            $scope.gotoPage = function(pageNo) {
                $scope.currentPage = pageNo;
                getListing()
            }

            function getParameterByName(name, url) {
                if (!url) url = window.location.href;
                name = name.replace(/[\[\]]/g, "\\$&");
                var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                    results = regex.exec(url);
                if (!results) return null;
                if (!results[2]) return '';
                return decodeURIComponent(results[2].replace(/\+/g, " "));
            }


        }
    ]);
})();