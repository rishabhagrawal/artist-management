(function() {
    'use strict';

    angular.module(globalConfig.appName).factory('ListingFactory', [
        '$http',
        '$q',
        function(
            $http,
            $q
        ) {
            var baseurl = window.baseUrl;
            
            return {
                allListing: allListing
            }

            function allListing(data) {
                var deferred = $q.defer();
                var req = {
                    method: "GET",
                    url: baseurl + 'api/artists',
                     params: {
                        appliedFilter: JSON.stringify(data)
                    }
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }
        }
    ]);

})();