<html>
	<head>
		<title><?php echo isset($title) ? $title : "Get My Artist" ?></title>
		<script src="<?php echo base_url('assets/js/client.vendor.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/client.app.min.js') ?>"></script>
		<link rel="stylesheet" href="<?php echo base_url('assets/css/client.vendor.min.css'); ?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/css/client.app.min.css'); ?>">
		<link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.ico') ?>" />
		<link rel="icon" href="<?php echo base_url('assets/images/favicon.ico') ?>" />
		<script type="text/javascript">
			window.baseUrl = "<?php echo base_url(); ?>";
		</script>
	</head>
	<body ng-app="getMyArtistApp">
	<header class="<?php echo ($this->uri->segment(1) == '') ? 'transparent' : '' ?>">
		<div class="header-wrapper">
			<div class="clearfix">
				<div class="logo-section">
					<a href="<?php echo base_url(); ?>">
						<img src="<?php echo base_url('assets/images/logo.png') ?>" alt="logo">
						<div class="logo-text">GET MY ARTIST</div>
					</a>
				</div>
				<div class="right-section">
					<ul class="clearfix">
						<li>
							<a href="<?php echo base_url('register') ?>">
								<!-- <?php 
								if($this->uri->segment(1) == ''){
								?>
								<img src="<?php echo base_url('assets/images/account-white.png') ?>" alt="user-img">
								<?php
								}else{
								?>
								<img src="<?php echo base_url('assets/images/account-blue.png') ?>" alt="user-img">
								<?php
								}
								?> -->
								ARTIST REGISTRATION
							</a>
						</li>
						<li>
							<a href="<?php echo base_url('requirement') ?>">
								<!-- <?php 
								if($this->uri->segment(1) == ''){
								?>
								<img src="<?php echo base_url('assets/images/login-white.png') ?>" alt="login-img">
								<?php
								}else{
								?>
								<img src="<?php echo base_url('assets/images/login-blue.png') ?>" alt="login-img">
								<?php
								}
								?> -->
								POST YOUR REQUIREMENT
							</a>
						</li>
						<li>
							<!-- <a href="#">
								<?php 
								if($this->uri->segment(1) == ''){
								?>
								<img src="<?php echo base_url('assets/images/menu-white.png') ?>" alt="menu-img">
								<?php
								}else{
								?>
								<img src="<?php echo base_url('assets/images/menu-black.png') ?>" alt="menu-img">
								<?php
								}
								?>
							</a> -->
						</li>
					</ul>
				</div>
			</div>
		</div>
	</header>
		
	