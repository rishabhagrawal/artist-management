(function() {
    'use strict';

    angular.module(globalConfig.appName).factory('RegisterFactory', [
        '$http',
        '$q',
        function(
            $http,
            $q
        ) {
            var baseurl = window.baseUrl;
            
            return {
                formSubmit: formSubmit
            }

            function formSubmit(data) {
                var deferred = $q.defer();
                var req = {
                    method: "POST",
                    url: baseurl + 'api/register',
                    data : data
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }
        }
    ]);

})();