<section id="register" ng-controller="registerController">
	<div class="top-banner clearfix">
		<div class="left-section">
			<h2>WE ARE LOOKING FOR <span>PERFORMERS</span></h2>
			<p>
				If you are an artist & believe in your craft.
				<br>
				Register on our listing.
				<br>
				Post your details, and our team shall get in touch with you.
			</p>
		</div>
	</div>
	<div class="form-wrapper">
		<div class="form-section">
			<h3>Fill in the following details to Register</h3>
			<div class="input-text">
				<input type="text" placeholder="Name" ng-model="formData.name" ng-class="{'error' : nameError}">
			</div>
			<ul class="clearfix">
				<li class="left-li">
					<div class="input-text">
						<select ng-change="categoryChoose()" ng-model="selectCategory" ng-options="data as data.name for data in categories track by data.id" ng-class="{'error' : categoryError}"></select>
					</div>
				</li>
				<li class="right-li">
					<div class="input-text">
						<select ng-change="locationChoose()" ng-model="selectLocation" ng-options="data as data.name for data in locations track by data.id" ng-class="{'error' : locationError}"></select>
					</div>
				</li>
			</ul>
			<div class="input-text">
				<textarea placeholder="Add Bio / Describe yourself" ng-model="formData.bio"></textarea>
			</div>
			<div class="input-text">
				<input type="text" placeholder="Add your Social Media Profile/Website" ng-model="formData.website">
			</div>
			<ul class="clearfix">
				<li class="left-li">
					<div class="input-text">
						<input type="text" placeholder="Phone Number" ng-model="formData.contact_no" ng-class="{'error' : contactError}">
					</div>
				</li>
				<li class="right-li">
					<div class="input-text">
						<input type="text" placeholder="Email Id." ng-model="formData.email" ng-class="{'error' : emailError}">
					</div>
				</li>
			</ul>
			<div class="submit-btn">
				<div class="success-msg" ng-show="successMsg">{{successMsg}}</div>
				<button ng-click="registerBtn()">REGISTER</button>
			</div>
		</div>
	</div>
</section>