(function() {
    'use strict';

    angular.module(globalConfig.appName).factory('RegisterRequestFactory', [
        '$http',
        '$q',
        function(
            $http,
            $q
        ) {
            var baseurl = window.baseUrl;
            return {
                getRegisterRequest: getRegisterRequest
            }

            function getRegisterRequest(data) {
                var deferred = $q.defer();
                var req = {
                    method: "GET",
                    url: baseurl + 'admin/api/register-request/list',
                    params : {
                        appliedFilter : JSON.stringify(data)
                    }
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }

        }
    ]);

})();
