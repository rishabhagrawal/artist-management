(function() {
    'use strict';

    angular.module(globalConfig.appName).factory('ContactUsFactory', [
        '$http',
        '$q',
        function(
            $http,
            $q
        ) {
            var baseurl = window.baseUrl;
            return {
                getContactUs: getContactUs
            }

            function getContactUs(data) {
                var deferred = $q.defer();
                var req = {
                    method: "GET",
                    url: baseurl + 'admin/api/contact-us/list',
                    params : {
                        appliedFilter : JSON.stringify(data)
                    }
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }

        }
    ]);

})();
