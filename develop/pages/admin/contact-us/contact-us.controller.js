(function() {
    'use strict';

    angular.module(globalConfig.appName).controller('ContactUsController', [
        '$scope',
        'ContactUsFactory',
        function(
            $scope,
            ContactUsFactory
        ) {

            $scope.currentPage = 1;
            $scope.count = 10;
            var applied_filter = {}
            $scope.isLoading = false;

            var loadResults = function() {
                applied_filter.page = $scope.currentPage;
                applied_filter.count = $scope.count;
                $scope.isLoading = true;
                ContactUsFactory.getContactUs(applied_filter).then(function(result) {
                    $scope.all_data = result.data.results;
                    $scope.total_result = result.data.total;
                    $scope.isLoading = false;
                }, function(response) {});
            };
            loadResults();

            $scope.paginate = function(isIncrement) {
                if (isIncrement) {
                    $scope.currentPage = $scope.currentPage + 1;
                } else {
                    $scope.currentPage = $scope.currentPage - 1;
                }
                loadResults();
            };

            // Search Filter section
            $scope.$watch('name', function(newVal, oldVal){
                if(newVal !== oldVal){
                    $scope.currentPage = 1;
                    applied_filter.name = newVal;
                    if(newVal === ''){
                        delete applied_filter.name;
                    }
                    loadResults();
                }
            },true);

            $scope.$watch('mobile_number', function(newVal, oldVal){
                if(newVal !== oldVal){
                    $scope.currentPage = 1;
                    applied_filter.mobile_number = newVal;
                    if(newVal === ''){
                        delete applied_filter.mobile_number;
                    }
                    loadResults();
                }
            },true);

        }
    ])

})();
