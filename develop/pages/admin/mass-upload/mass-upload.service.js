(function() {
    'use strict';

    angular.module(globalConfig.appName).factory('MassUploadFactory', [
        '$http',
        '$q',
        function(
            $http,
            $q
        ) {
            var baseurl = window.baseUrl;
            return {
                uploadExcel: uploadExcel
            }

            function uploadExcel(data) {
                var deferred = $q.defer();
                var req = {
                    method: "POST",
                    url: baseurl + 'admin/api/mass-upload',
                    data : data,
                    headers: {
                        'Content-Type': undefined
                    }
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }

        }
    ]);

})();
