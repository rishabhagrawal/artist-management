(function() {
    'use strict';

    angular.module(globalConfig.appName).controller('massUploadController', [
        '$scope',
        'MassUploadFactory',
        function(
            $scope,
            MassUploadFactory
        ) {

            $scope.isfileChoose = false;
            $scope.fileData = new FormData;
            $scope.chooseImage = function(file) {
                if (file[0]) {
                    $scope.isfileChoose = true;
                } else {
                    $scope.isfileChoose = false;
                }
                $scope.fileData.append('file', file[0]);
                $scope.$apply();
            }

            $scope.submitBtn = function() {
                if ($scope.isfileChoose) {
                    $scope.isSubmitting = true;
                    MassUploadFactory.uploadExcel($scope.fileData).then(function(result) {
                        $scope.isSubmitting = false;
                        $scope.formSuccess = true;
                    }, function(error) {

                    })
                }
            }

        }
    ])

})();
