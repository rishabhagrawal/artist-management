<section class="main-section mass-upload"  ng-controller="massUploadController">
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4>Mass Upload</h4>
			</div>
		</div>
		<div class="breadcrumb-line">
			<ul class="breadcrumb">
				<li>
					<a href="#">
						<i class="icon-home"></i>
						Home
					</a>
				</li>
				<li class="active">
					Mass Upload
				</li>
			</ul>
		</div>
	</div>
	<article>
		<div class="choose-excel">
			<label>
				<input type="file" ng-files="chooseImage($files)">
			</label>
		</div>
		<div class="submit-btn">
			<button ng-click="submitBtn()" ng-class="{'in-active': isSubmitting}">SUBMIT</button>
			<img src="<?php echo base_url('assets/images/ring.gif'); ?>" alt="loading" ng-show="isSubmitting">
		</div>
		<div class="success" ng-show="formSuccess">
			File has been successfully Uploaded.
		</div>
	</article>
</section>