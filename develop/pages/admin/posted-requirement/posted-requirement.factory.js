(function() {
    'use strict';

    angular.module(globalConfig.appName).factory('PostReqFactory', [
        '$http',
        '$q',
        function(
            $http,
            $q
        ) {
            var baseurl = window.baseUrl;
            return {
                getPostReq: getPostReq
            }

            function getPostReq(data) {
                var deferred = $q.defer();
                var req = {
                    method: "GET",
                    url: baseurl + 'admin/api/post-requirement/list',
                    params : {
                        appliedFilter : JSON.stringify(data)
                    }
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }

        }
    ]);

})();
