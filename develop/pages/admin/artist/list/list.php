<section class="main-section" id="artist-list" ng-controller="artistListController">
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4>My Artist</h4>
			</div>
			<div class="right-section">
				<a href="<?php echo base_url('admin/new-artist'); ?>">
					<button>Add New</button>
				</a>
			</div>
		</div>
		<div class="breadcrumb-line">
			<ul class="breadcrumb">
				<li>
					<a href="#">
						<i class="icon-home"></i>
						Home
					</a>
				</li>
				<li class="active">
					Artist
				</li>
			</ul>
		</div>
	</div>
	<article>
		<div class="filter-section">
			<ul class="row">
				<li>
					<input type="text" ng-model="artist_name" placeholder="Type Name">
					<i class="icon-cancel-circle" ng-show="artist_name.length > 0" ng-click="removeFilter('artist_name')" ng-cloak></i>
				</li>
				<li>
					<select ng-change="selectCategory()" ng-model="selectedCategory">
						<option value="">Select Category</option>
						<option ng-repeat="data in categories" value="{{data.id}}">{{data.name}}</option>
					</select>
					<i class="icon-cancel-circle" ng-show="selectedCategory != ''" ng-click="removeSelectFilter('category')" ng-cloak></i>
				</li>
				<li>
					<select ng-change="selectLocation()" ng-model="selectedLocation">
						<option value="">Select Location</option>
						<option ng-repeat="data in loactions" value="{{data.id}}">{{data.name}}</option>
					</select>
					<i class="icon-cancel-circle" ng-show="selectedLocation != ''" ng-click="removeSelectFilter('location')" ng-cloak></i>
				</li>
			</ul>
		</div>
		<div class="table-section">
			<div class="total-results">
				<div ng-cloak><span>{{totalArtist || 0}}</span> Result{{totalArtist > 1 ? 's' : ''}} Found</div>
			</div>
			<div class="table-wrapper">
				<div class="table-head">
					<ul>
						<li>S.No.</li>
						<li>Name</li>
						<li>Category</li>
						<li>Location</li>
						<li>Live Date</li>
						<li>Status</li>
						<li>Action</li>
					</ul>
				</div>
				<div class="table-body">
					<ul ng-cloak ng-repeat="list in allArtists" ng-class="{'is-deleted' : list.deleteConfirm}" ng-if="!list.deleted">
						<li ng-cloak>{{(currentPage-1) * count + $index + 1}}</li>
						<li ng-cloak>{{list.name}}</li>
						<li ng-cloak>{{list.category}}</li>
						<li ng-cloak>{{list.location}}</li>
						<li class="date-time">
							<div class="date" ng-cloak>{{list.live_date}}</div>
							<div class="time">01:20 AM</div>
						</li>
						<li ng-cloak class="status">
							<div ng-class="{'pause' : list.is_pause, 'live' : !list.is_pause}">{{list.is_pause ? "PAUSE" : "LIVE"}}</div>
						</li>
						<li class="action">
							<div class="action-sction" ng-cloak ng-class="{'deleteConfirm':list.deleteConfirm}">
								<a href="<?php echo base_url('admin/artist-detail/{{list.id}}') ?>" class="edit-icon-a"><i class="icon-pencil2 edit-icon" ng-click="edit(list)"></i></a>
								<i ng-class="{'icon-play3 play-icon': list.is_pause, 'icon-pause2 pause-icon': !list.is_pause}" ng-click="pause(list)"></i>
								<i class="icon-link external-link"></i>
								<i class="icon-bin delete-icon" ng-click="delete(list)"></i>
							</div>
							<div class="delete-confirm" ng-cloak ng-class="{'deleteConfirm':list.deleteConfirm}">
								<div class="yes" ng-click="finalDelete(list)">Delete</div>
								<div class="no" ng-click="deleteCancel(list)">Cancel</div>
							</div>
						</li>
					</ul>
					<div class="no-result-found" ng-cloak ng-show="totalArtist == 0">
						<i class="icon-warning"></i>
						No Result Found !
					</div>
					<div class="loading" ng-show="isLoading">
						<i class="icon-spinner9"></i>
					</div>
				</div>
			</div>
			<div class="table-paggination row" ng-cloak ng-show="totalArtist > 10">
				<ul>
					<li>
						<button ng-click="paginate(false)" ng-disabled="currentPage == 1">Previous</button>
					</li>
					<li ng-cloak>{{currentPage}}</li>
					<li>
						<button ng-click="paginate(true)" ng-disabled="currentPage * count >= totalArtist">Next</button>
					</li>
				</ul>
			</div>
		</div>
	</article>
</section>