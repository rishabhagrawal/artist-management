(function() {
    'use strict';

    angular.module(globalConfig.appName).controller('artistListController', [
        '$scope',
        'ArtistFactory',
        function(
            $scope,
            ArtistFactory
        ) {

            $scope.currentPage = 1;
            $scope.count = 10;
            var applied_filter = {}
            $scope.isLoading = false;
            $scope.selectedCategory = '';
            $scope.selectedLocation = '';

            ArtistFactory.allLoactions().then(function(result) {
                $scope.loactions = result.data.locations;
            }, function(error) {});
            ArtistFactory.allcategories().then(function(result) {
                $scope.categories = result.data.categories;
            }, function(error) {});

            var artistResult = function() {
                applied_filter.page = $scope.currentPage;
                applied_filter.count = $scope.count;
                $scope.isLoading = true;
                ArtistFactory.getAllArtist(applied_filter).then(function(result) {
                    $scope.allArtists = result.data.artists;
                    $scope.totalArtist = result.data.total;
                    $scope.isLoading = false;
                }, function(response) {});
            };
            artistResult();

            $scope.paginate = function(isIncrement) {
                if (isIncrement) {
                    $scope.currentPage = $scope.currentPage + 1;
                } else {
                    $scope.currentPage = $scope.currentPage - 1;
                }
                artistResult();
            };

            $scope.removeFilter = function(filter_name) {
                delete applied_filter[filter_name];
                $scope[filter_name] = "";
                artistResult();
            }
            $scope.removeSelectFilter = function(data) {
                if (data == 'category') {
                    $scope.selectedCategory = '';
                    delete applied_filter.category;
                }
                if (data == 'location') {
                    $scope.selectedLocation = '';
                    delete applied_filter.location;
                }
                artistResult();
            }

            // Search Filter section
            $scope.$watch('artist_name', function(newVal, oldVal) {
                if (newVal !== oldVal) {
                    $scope.currentPage = 1;
                    applied_filter.name = newVal;
                    $scope.isArtistNameCloseShow = true;
                    if (newVal === '') {
                        delete applied_filter.name;
                        $scope.isArtistNameCloseShow = false;
                    }
                    artistResult();
                }
            }, true);

            $scope.$watch('mobile_number', function(newVal, oldVal) {
                if (newVal !== oldVal) {
                    $scope.currentPage = 1;
                    applied_filter.mobile_number = newVal;
                    if (newVal === '') {
                        delete applied_filter.mobile_number;
                    }
                    artistResult();
                }
            }, true);

            $scope.selectCategory = function() {
                $scope.currentPage = 1;
                if ($scope.selectedCategory != '') {
                    applied_filter.category = parseInt($scope.selectedCategory);
                } else {
                    delete applied_filter.category;
                }
                artistResult();
            }
            $scope.selectLocation = function() {
                $scope.currentPage = 1;
                if ($scope.selectedLocation != '') {
                    applied_filter.location = parseInt($scope.selectedLocation);
                } else {
                    delete applied_filter.location;
                }
                artistResult();
            }

            // $scope.$watch('email', function(newVal, oldVal){
            //     if(newVal !== oldVal){
            //         $scope.currentPage = 1;
            //         applied_filter.email = newVal;
            //         if(newVal === ''){
            //             delete applied_filter.email;
            //         }
            //         debugger;
            //         searchResult();
            //     }
            // },true);
            // Search Filter section


            $scope.edit = function(data) {

            }

            $scope.pause = function(data) {
                data.is_pause = !data.is_pause;
                // api call for Pause Artist
                var reqObj = {
                    artist_id: data.id
                }
                ArtistFactory.pauseArtist(reqObj).then(function(result) {}, function(response) {});
            }

            $scope.delete = function(data) {
                data.deleteConfirm = true;
            }

            $scope.finalDelete = function(data) {
                data.deleted = true;
                //api call for delete artist
                var reqObj = {
                    artist_id: data.id
                }
                ArtistFactory.deleteArtist(reqObj).then(function(result) {}, function(response) {});
            }

            $scope.deleteCancel = function(data) {
                data.deleteConfirm = false;
            }
        }
    ])

})();
