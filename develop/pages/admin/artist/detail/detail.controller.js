(function() {
    'use strict';

    angular.module(globalConfig.appName).controller('updateArtistController', [
        '$scope',
        'ArtistFactory',
        function(
            $scope,
            ArtistFactory
        ) {

            $scope.isImageChoose = false;
            $scope.formData = {
                name: "",
                email: "",
                website_link: "",
                location_id: "",
                category_id: "",
                landline_number: "",
                facebook_link: "",
                twitter_link: "",
                instagram_link: "",
                contact_person_name: "",
                contact_person_email: "",
                contact_person_mobile: "",
                video_link: "",
                bio: "",
                is_featured: 0
            }


            var url = window.location.href;
            var artist_id = parseInt(url.substr(url.lastIndexOf('/') + 1));
            ArtistFactory.getArtistData(artist_id).then(function(result) {
                $scope.formData = result.data.artist_data;
            }, function(error) {});

            ArtistFactory.allLoactions().then(function(result) {
                $scope.loactions = result.data.locations;
            }, function(error) {});
            ArtistFactory.allcategories().then(function(result) {
                $scope.categories = result.data.categories;
            }, function(error) {});


            $scope.imageData = new FormData;
            $scope.chooseImage = function(image) {
                if (image[0]) {
                    $scope.isImageChoose = true;
                } else {
                    $scope.isImageChoose = false;
                }
                $scope.imageData.append('image', image[0]);
                $scope.$apply();
            }

            $scope.removeImage = function() {
                $scope.isImageChoose = false;
                $scope.imageData = new FormData;
            }

            $scope.submitBtn = function() {
                $scope.isSubmitting = false;
                var isValid = true;
                if (!$scope.formData.name) {
                    isValid = false;
                    $scope.nameError = true;
                }
                if (!$scope.formData.location_id) {
                    isValid = false;
                    $scope.locationError = true;
                }
                if (!$scope.formData.category_id) {
                    isValid = false;
                    $scope.categoryError = true;
                }
                if (!$scope.formData.bio) {
                    isValid = false;
                    $scope.bioError = true;
                }
                if (isValid) {
                    $scope.isSubmitting = true;

                    ArtistFactory.updateArtist($scope.formData).then(function(result) {

                        if ($scope.isImageChoose) {
                            $scope.imageData.append('artist_id', result.data.artist_id);
                            ArtistFactory.updateArtistImage($scope.imageData).then(function(imageResult) {
                                $scope.isSubmitting = false;
                                window.location.href = window.baseUrl + "admin/artists";
                            }, function(imageError) {})
                        } else {
                            $scope.isSubmitting = false;
                            window.location.href = window.baseUrl + "admin/artists";
                        }
                    }, function(error) {

                    })
                }
            }

            $scope.loadTags = function(query) {
                return $scope.loactions;
            };

            $scope.$watch('formData', function(oldVal, newVal) {
                if (newVal != oldVal) {
                    $scope.nameError = false;
                    $scope.locationError = false;
                    $scope.categoryError = false;
                    $scope.bioError = false;
                }
            }, true)

            $scope.featureChoose = function(data) {
                $scope.formData.is_featured = data;
            }

        }
    ])

})();
