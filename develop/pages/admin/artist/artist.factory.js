(function() {
    'use strict';

    angular.module(globalConfig.appName).factory('ArtistFactory', [
        '$http',
        '$q',
        function(
            $http,
            $q
        ) {
            var baseurl = window.baseUrl;
            return {
                getAllArtist: getAllArtist,
                pauseArtist: pauseArtist,
                deleteArtist: deleteArtist,
                allLoactions: allLoactions,
                allcategories: allcategories,
                addNewArtist: addNewArtist,
                updateArtist: updateArtist,
                updateArtistImage : updateArtistImage,
                getArtistData : getArtistData, 
            }

            function getAllArtist(data) {
                var deferred = $q.defer();
                var req = {
                    method: "GET",
                    url: baseurl + 'admin/api/artist/list',
                    cache: true,
                    params: {
                        appliedFilter: JSON.stringify(data)
                    }
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }

            function pauseArtist(data) {
                var deferred = $q.defer();
                var req = {
                    method: "GET",
                    url: baseurl + 'admin/api/artist/pause',
                    params: data
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }

            function deleteArtist(data) {
                var deferred = $q.defer();
                var req = {
                    method: "GET",
                    url: baseurl + 'admin/api/artist/delete',
                    params: data
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }

            function allLoactions(data) {
                var deferred = $q.defer();
                var req = {
                    method: "GET",
                    url: baseurl + 'api/constant/locations',
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }

            function allcategories(data) {
                var deferred = $q.defer();
                var req = {
                    method: "GET",
                    url: baseurl + 'api/constant/categories',
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }

            function addNewArtist(data) {
                var deferred = $q.defer();
                var req = {
                    method: "POST",
                    url: baseurl + 'admin/api/artist/add',
                    data: data
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }

            function updateArtist(data) {
                var deferred = $q.defer();
                var req = {
                    method: "PUT",
                    url: baseurl + 'admin/api/artist/edit',
                    data: data
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }

            function updateArtistImage(data) {
                var deferred = $q.defer();
                var req = {
                    method: "POST",
                    url: baseurl + 'admin/api/artist/images',
                    data: data,
                    headers: {
                        'Content-Type': undefined
                    }
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }

            function getArtistData(aritst_id) {
                var deferred = $q.defer();
                var req = {
                    method: "GET",
                    url: baseurl + 'admin/api/artist/details/'+aritst_id,
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }

            

        }
    ]);

})();
