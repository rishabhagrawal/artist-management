(function() {
    'use strict';

    angular.module(globalConfig.appName).factory('AdminLoginFactory', [
        '$http',
        '$q',
        function(
            $http,
            $q
        ) {
            var baseurl = window.baseUrl;
            return {
                login: login,
            }

            function login(data) {
                var deferred = $q.defer();
                var req = {
                    method: "POST",
                    url: baseurl + 'admin/api/auth/login',
                    data: data
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }
        }
    ]);

})();
