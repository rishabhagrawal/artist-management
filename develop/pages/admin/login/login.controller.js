(function() {
    'use strict';
    angular.module(globalConfig.appName).controller('loginController', [
        '$scope',
        'AdminLoginFactory',
        function(
            $scope,
            AdminLoginFactory
        ) {

            $scope.errorMsgShow = false;
            $scope.formData = {
                email_id: "",
                password: ""
            }

            function validateEmail(email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }

            $scope.login = function() {
                var valid = true;
                $scope.errorMsg = "Email Id or Password not Valid !";

                if (!$scope.formData.email_id) {
                    $scope.errorMsgShow = true;
                    $scope.emailError = true;
                    valid = false;
                }
                if ($scope.formData.email_id) {
                    if (!validateEmail($scope.formData.email_id)) {
                        $scope.errorMsgShow = true;
                        $scope.errorMsg = "Email Id not Valid !";
                        $scope.emailError = true;
                        valid = false;
                    }
                }
                if (!$scope.formData.password) {
                    $scope.errorMsgShow = true;
                    $scope.passwordError = true;
                    valid = false;
                }
                if (valid) {
                    AdminLoginFactory.login($scope.formData).then(function(result) {
                        if (result.data.status == "fail") {
                            $scope.errorMsgShow = true;
                        } else {
                            window.location.href = window.baseUrl + "admin/dashboard";
                        }
                    }, function(response) {

                    });
                }
            }

            $scope.$watch('formData', function(newVal, oldVal) {
                if (newVal != oldVal) {
                    $scope.errorMsgShow = false;
                    $scope.emailError = false;
                    $scope.passwordError = false;
                }
            }, true)

        }
    ]);
})();
