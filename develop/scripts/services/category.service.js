(function() {
    'use strict';

    angular.module(globalConfig.appName).factory('CategoryFactory', [
        '$http',
        '$q',
        function(
            $http,
            $q
        ) {
            var baseurl = window.baseUrl;
            return {
                getAllCategory: getAllCategory
            }
            function getAllCategory(data) {
                var deferred = $q.defer();
                var req = {
                    method: "GET",
                    url: baseurl + 'api/constant/categories',
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }
        }
    ]);

})();