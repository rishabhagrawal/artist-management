(function() {
    'use strict';

    angular.module(globalConfig.appName).factory('LocationFactory', [
        '$http',
        '$q',
        function(
            $http,
            $q
        ) {

            var baseurl = window.baseUrl;
            
            return {
                getAllCities: getAllCities
            }
            
            function getAllCities(data) {
                var deferred = $q.defer();
                var req = {
                    method: "GET",
                    url: baseurl + 'api/constant/locations',
                }
                $http(req)
                    .then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }
        }
    ]);

})();