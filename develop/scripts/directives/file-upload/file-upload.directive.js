(function() {

    'use strict';

    angular.module(globalConfig.appName).directive('ngFiles', ['$parse', function($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var onChange = $parse(attrs.ngFiles);
                
                element.on('change', function(event) {
                    onChange(scope, { $files: event.target.files });
                });
            }
        };
    }]);
})();
