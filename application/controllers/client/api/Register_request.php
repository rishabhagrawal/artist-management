<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Register_request extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('client/api/register_request_model');
	}

	function save() {
		$data = $this->register_request_model->saveRegisterRequest();
		json_output($data);
	}
}