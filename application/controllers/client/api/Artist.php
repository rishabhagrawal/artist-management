<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Artist extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('client/api/artist_model');
	}

	function featured() {
		$data = $this->artist_model->featured();
		json_output($data);
	}

	function all_list() {
		$data = $this->artist_model->all_list();
		json_output($data);
	}

	function detail($artist_id) {
		$data = $this->artist_model->detail($artist_id);
		json_output($data);
	}

}