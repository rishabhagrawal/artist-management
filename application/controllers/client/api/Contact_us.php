<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('client/api/contact_us_model');
	}

	function save() {
		$data = $this->contact_us_model->saveContactUs();
		json_output($data);
	}
}