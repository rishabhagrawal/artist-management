<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Post_requirement extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('client/api/post_requirement_model');
	}

	function save() {
		$data = $this->post_requirement_model->savePostRequirement();
		json_output($data);
	}
}