<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('home_model');
	}

	function index() {
		$data['title'] = "Home | Get My Artist";
		load_client_page('home/home', $data);
	}
	
	public function register(){
		$data['title'] = "Register | Get My Artist";
		load_client_page('register/register', $data);
	}

	public function requirement(){
		$data['title'] = "Requirement | Get My Artist";
		load_client_page('requirement/requirement', $data);
	}

	public function listing(){
		$data['title'] = "Artist Listing | Get My Artist";
		load_client_page('listing/listing', $data);
	}

	public function detail(){
		$data['title'] = "Artist Detail | Get My Artist";
		load_client_page('detail/detail', $data);
	}

	public function aboutUs(){
		$data['title'] = "About Us | Get My Artist";
		load_client_page('about-us/about-us', $data);
	}
}