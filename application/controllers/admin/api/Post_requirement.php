<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Post_requirement extends CI_Controller{

	public function __construct(){
		parent::__construct();
		if(get_session('admin_logged_in') != 1){
			redirect(base_url('admin/login'));
		}
		$this->load->model('admin/api/post_requirement_model');
	}

	function all_list() {
		$data = $this->post_requirement_model->all_list();
		json_output($data);
	}
}