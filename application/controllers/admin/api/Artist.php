<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Artist extends CI_Controller{

	public function __construct(){
		parent::__construct();
		if(get_session('admin_logged_in') != 1){
			redirect(base_url('admin/login'));
		}
		$this->load->model('admin/api/artist_model');
	}

	function all_list() {
		$data = $this->artist_model->all_list();
		json_output($data);
	}

	function pause() {
		$data = $this->artist_model->pause();
		json_output($data);
	}

	function delete() {
		$data = $this->artist_model->delete();
		json_output($data);
	}

	function add_artist() {
		$data = $this->artist_model->add_artist();
		json_output($data);
	}

	function edit_artist() {
		$data = $this->artist_model->edit_artist();
		json_output($data);
	}

	function images() {
		$data = $this->artist_model->images();
		json_output($data);
	}

	function get_artist_detail($artist_id) {
		$data = $this->artist_model->get_artist_detail($artist_id);
		json_output($data);
	}

	function delete_image($artist_id) {
		$data = $this->artist_model->delete_image($artist_id);
		json_output($data);
	}


}