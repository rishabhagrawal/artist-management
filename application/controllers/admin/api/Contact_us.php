<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller{

	public function __construct(){
		parent::__construct();
		if(get_session('admin_logged_in') != 1){
			redirect(base_url('admin/login'));
		}
		$this->load->model('admin/api/contact_us_model');
	}

	function all_list() {
		$data = $this->contact_us_model->all_list();
		json_output($data);
	}
}