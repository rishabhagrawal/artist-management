<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mass_upload extends CI_Controller{

	public function __construct(){
		parent::__construct();
		if(get_session('admin_logged_in') != 1){
			redirect(base_url('admin/login'));
		}
		$this->load->model('admin/api/mass_upload_model');
	}

	function upload_file() {
		$data = $this->mass_upload_model->upload_file();
		json_output($data);
	}
}