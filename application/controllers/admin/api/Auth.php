<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('admin/api/auth_model');
    }

    function index(){

    }

    function login(){
        $data = $this->auth_model->login();
        json_output($data);
    }

    function logout(){
        $data = $this->auth_model->logout();
        json_output($data);
    }

}