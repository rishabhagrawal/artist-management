<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(get_session('admin_logged_in') == 1){
			redirect(base_url('admin/dashboard'));
		}
	}

	function index() {
		$data['title'] = "Get My Artist | Login";
		load_view("admin/login/login", $data);
	}

}