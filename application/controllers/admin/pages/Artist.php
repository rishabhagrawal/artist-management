<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Artist extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(get_session('admin_logged_in') != 1){
			redirect(base_url('admin/login'));
		}
	}

	function index() {
	}
	
	function artist_new() {
		$data['title'] = "Get My Artist | Add New Artist";
		$data['page'] = "artist/new/new";
		load_admin_page($data);
	}

	function artist_list() {
		$data['title'] = "Get My Artist | Artist";
		$data['page'] = "artist/list/list";
		load_admin_page($data);
	}

	function artist_detail($id) {
		$data['title'] = "Get My Artist | Artist Detail";
		$data['page'] = "artist/detail/detail";
		load_admin_page($data);
	}


}