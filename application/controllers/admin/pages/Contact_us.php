<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(get_session('admin_logged_in') != 1){
			redirect(base_url('admin/login'));
		}
	}

	function index() {
		$data['title'] = "Get My Artist | Contact Us";
		$data['page'] = "contact-us/contact-us";
		load_admin_page($data);
	}

}