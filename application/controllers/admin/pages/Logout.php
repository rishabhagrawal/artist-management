<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('admin/api/auth_model');
	}

	function index() {
		$data = $this->auth_model->logout();
		redirect("admin/login");
	}

}