<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(get_session('admin_logged_in') != 1){
			redirect(base_url('admin/login'));
		}
	}

	function index() {
		$data['title'] = "Get My Artist | Dashboard";
		$data['page'] = "dashboard/dashboard";
		load_admin_page($data);
	}
}