<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Constant extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('constant_model');
	}

	function locations() {
		$data = $this->constant_model->locations();
		json_output($data);
	}

	function categories() {
		$data = $this->constant_model->categories();
		json_output($data);
	}

	function artistsCategories(){
		$data = $this->constant_model->artistsCategories();
		json_output($data);
	}

}