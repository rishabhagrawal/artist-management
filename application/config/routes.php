<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|			my-controller/my-method	-> my_controller/my_method
*/

// CLIENT ROUTES
// $route['default_controller'] = 'home';
$route['default_controller'] = 'home';

// ****************
// Client Pages
// ****************
// $route['home']['GET'] = 'home';
$route['register']['GET'] = 'home/register';
$route['requirement']['GET'] = 'home/requirement';
$route['about-us']['GET'] = 'home/aboutUs';
$route['listing']['GET'] = 'home/listing';
$route['listing/(:any)']['GET'] = 'home/listing';
$route['listing/(:any)/(:any)']['GET'] = 'home/listing';
$route['listing/(:any)/(:any)/(:any)']['GET'] = 'home/listing';
$route['listing/(:any)/(:any)/(:any)/(:any)']['GET'] = 'home/listing';
$route['listing/(:any)/(:any)/(:any)/(:any)/(:any)']['GET'] = 'home/listing';
$route['listing/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)']['GET'] = 'home/listing';
$route['listing/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)']['GET'] = 'home/listing';
$route['listing/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)']['GET'] = 'home/listing';
$route['listing/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)']['GET'] = 'home/listing';
$route['artist-detail/(:any)']['GET'] = 'home/detail/$1';


// **************
// Admin Pages
// **************
$route['admin']['GET'] = 'admin/pages/login';
$route['admin/login']['GET'] = 'admin/pages/login';
$route['admin/logout']['GET'] = 'admin/pages/logout';
$route['admin/dashboard']['GET'] = 'admin/pages/dashboard';
$route['admin/artists']['GET'] = 'admin/pages/artist/artist_list';
$route['admin/new-artist']['GET'] = 'admin/pages/artist/artist_new';
$route['admin/artist-detail/(:any)']['GET'] = 'admin/pages/artist/artist_detail/$1';
$route['admin/register-request']['GET'] = 'admin/pages/register_request';
$route['admin/posted-requirement']['GET'] = 'admin/pages/posted_requirement';
$route['admin/contact-us']['GET'] = 'admin/pages/contact_us';
$route['admin/mass-upload']['GET'] = 'admin/pages/mass_upload';


// **************
// Admin API
// **************
$route['admin/api/auth/login']['POST'] = 'admin/api/auth/login';
$route['admin/api/auth/logout']['GET'] = 'admin/api/auth/logout';

$route['admin/api/artist/list']['GET'] = 'admin/api/artist/all_list';
$route['admin/api/artist/pause']['GET'] = 'admin/api/artist/pause';
$route['admin/api/artist/delete']['GET'] = 'admin/api/artist/delete';
$route['admin/api/artist/add']['POST'] = 'admin/api/artist/add_artist';
$route['admin/api/artist/edit']['PUT'] = 'admin/api/artist/edit_artist';
$route['admin/api/artist/images']['POST'] = 'admin/api/artist/images';
$route['admin/api/artist/details/(:any)']['GET'] = 'admin/api/artist/get_artist_detail/$1';
$route['admin/api/artist/delete-image/(:any)']['GET'] = 'admin/api/artist/delete_image/$1';


$route['admin/api/post-requirement/list']['GET'] = 'admin/api/post_requirement/all_list';
$route['admin/api/register-request/list']['GET'] = 'admin/api/register_request/all_list';
$route['admin/api/contact-us/list']['GET'] = 'admin/api/contact_us/all_list';


$route['admin/api/mass-upload']['POST'] = 'admin/api/mass_upload/upload_file';

$route['api/constant/locations']['GET'] = 'constant/locations';
$route['api/constant/categories']['GET'] = 'constant/categories';
$route['api/artists-categories']['GET'] = 'constant/artistsCategories';


$route['api/contact-us']['POST'] = 'client/api/contact_us/save';
$route['api/register']['POST'] = 'client/api/register_request/save';
$route['api/requirement']['POST'] = 'client/api/post_requirement/save';
$route['api/featured-artists']['GET'] = 'client/api/artist/featured';
$route['api/artists']['GET'] = 'client/api/artist/all_list';
$route['api/artist-detail/(:any)']['GET'] = 'client/api/artist/detail/$1';


// $route['404_override'] = 'welcome/error_404';
$route['translate_uri_dashes'] = FALSE;










