<?php

function get_title($title, $trailing = true) {
	if( $trailing ) $title .= ' - '.SITE_NAME;
	return $title;
}


function user_logged_in( $redirect = '' ) {
	if ( get_session('logged_in') == 1 ) {
		if( !empty($redirect) ) redirect($redirect);
		return true;
	} else {
		return false;
	}
}

function get_value($field, $table, $value, $where='id') {
	$CI =& get_instance();
	$output = false;
	
	$CI->db->select($field);
	$CI->db->from($table);
	$CI->db->where($where, $value);
	$query = $CI->db->get();
	if( $query->num_rows() > 0 ) {
		$row = $query->row_array();
		$output = $row[$field];
	}
	return $output;
}

function set_value($field, $value, $table, $where_value, $where_cond = 'id') {
	$CI =& get_instance();
	
	$CI->db->set($field, $value);
	$CI->db->where($where_cond, $where_value);
	$result = $CI->db->update($table);
	return $result;
}

function get_row($table, $id, $where='id') {
	$CI =& get_instance();
	$CI->db->from($table);
	$CI->db->where($where, $id);
	$query = $CI->db->get();
	if( $query->num_rows() > 0 ) {
		$result = $query->row_array();
		return $result;
	}
	return false;
}


function get_table($table, $value='', $where='') {
	$CI =& get_instance();
	if( !empty($where) ) {
		$CI->db->where($where, $value);
	}
	$query = $CI->db->get($table);
	if( $query->num_rows() > 0 ) {
		$result = $query->result_array();
		return $result;
	}
	return false;
}

function load_view($view, $data = NULL){
    $CI =& get_instance();
    $CI->load->view($view, $data);
}

function compare_datetime($a, $b) {
	$ad = strtotime($a['exact_date']);
	$bd = strtotime($b['exact_date']);

	if ($ad == $bd) {
		return 0;
	}

	return $ad > $bd ? 1 : -1;
}

function remove_dir($dir) {
   if (is_dir($dir)) {
     $objects = scandir($dir);
     foreach ($objects as $object) {
       if ($object != "." && $object != "..") {
         if (filetype($dir."/".$object) == "dir"){
            remove_dir($dir."/".$object);
         } else { 
            unlink($dir."/".$object);
         }
       }
     }
     reset($objects);
     rmdir($dir);
  }
}

function get_extention($file) {
	return pathinfo($file['name'], PATHINFO_EXTENSION);
}

function custom_encode($string) {
	$key = "TeStInG";
	$string = base64_encode($string);
	$string = str_replace('=', '', $string);
	$main_arr = str_split($string);
	$output = array();
	$count = 0;
	for( $i=0; $i<strlen($string); $i++) {
		$output[] = $main_arr[$i];
		if($i%2==1) {
			$output[] = substr($key, $count, 1);
			$count++;
		}
	}
	$string = implode('', $output);
	return $string;
}

function custom_decode($string) {
	$key = "TeStInG";
	$arr = str_split($string);
	$count = 0;
	for( $i=0; $i<strlen($string); $i++) {
		if( $count < strlen($key) ) {
			if($i%3==2) {
				unset($arr[$i]);
				$count++;
			}
		}
	}
	$string = implode('', $arr);
	$string = base64_decode($string);
	return $string;
}

function i_encode($url) {
	$CI =& get_instance();
	$uri = $CI->encrypt->encode($url);
	$pattern = '"/"';
	$new_uri = preg_replace($pattern, '_', $uri);
	return $new_uri;
}

function i_decode($url) {
	$CI =& get_instance();
	$pattern = '"_"';
	$uri = preg_replace($pattern, '/', $url);
	$new_uri = $CI->encrypt->decode($uri);
	return $new_uri;
}

function get_array_key($value, $array) {
	while ($single = current($array)) {
		if ($single == $value) {
			return key($array);
		}
		next($array);
	}
}

function set_session($name, $value) {
	$CI =& get_instance();
	$CI->session->set_userdata($name, $value);
}

function set_sessions($array) {
	$CI =& get_instance();
	$CI->session->set_userdata($array);
}

function get_session($name='') {
	$CI =& get_instance();
	if( !empty($name) ) {
		return $CI->session->userdata($name);
	}
	return $CI->session->userdata();
}

function unset_session($name) {
	$CI =& get_instance();
	$CI->session->unset_userdata($name);
}

function getFriendlyURL($string, $separator='-') {
	$accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
	$special_cases = array( '&' => 'and', "'" => '');
	$string = strtolower( trim( $string ));
	$string = str_replace( array_keys($special_cases), array_values( $special_cases), $string );
	$string = preg_replace( $accents_regex, '$1', htmlentities( $string, ENT_QUOTES, 'UTF-8' ) );
	$string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
	$string = preg_replace("/[$separator]+/u", "$separator", $string);
	return $string;
}


function load_client_page($page,$data){
	$CI =& get_instance();
	load_view('client/header/header', $data);
	load_view('client/'.$page);
	load_view('client/footer/footer');
}

// load content with header, left, footer
function load_frontend_page($views, $data = array()) {
	$CI =& get_instance();
	
	$inclusions = $data['inclusions'];

	$inclusions['js'][] = 'assets/js/frontend/main';

	$inclusions['css'][] = 'assets/css/font-awesome.min';
	$inclusions['css'][] = 'assets/css/frontend/style';
	$inclusions['css'][] = 'assets/css/frontend/responsive';

	$data['inclusions'] = array_merge($data['inclusions'], $inclusions);
	$CI->parser->parse('frontend/layout/header', $data);
	
	if( !is_array($views) ) $views = array($views);
	foreach( $views as $view ) {
		load_view($view);
	}
	
	load_view('frontend/layout/footer');
}

function load_backend_page($views, $data = array()) {
	$CI =& get_instance();
	
	if( sizeof($data['inclusions']) == 0 ) {
		$inclusions = inclusions();
	} else {
		$inclusions = $data['inclusions'];
	}
	
	$inclusions['css'][] = 'assets/css/bootstrap.min';
	$inclusions['css'][] = 'assets/css/font-awesome.min';
	$inclusions['css'][] = 'assets/css/backend/skin-blue';
	$inclusions['css'][] = 'assets/css/constants';
	$inclusions['css'][] = 'assets/css/backend/style';
	$inclusions['css'][] = 'assets/css/backend/responsive';
	
	$inclusions['js'][] = 'assets/js/bootstrap.min';
	$inclusions['js'][] = 'assets/js/backend/app.min';
	$inclusions['js'][] = 'assets/js/backend/main';

	$data['inclusions'] = array_merge($data['inclusions'], $inclusions);
	$CI->parser->parse('backend/layout/header', $data);
	
	load_view('backend/layout/menu');

	if( !is_array($views) ) $views = array($views);
	foreach( $views as $view ) {
		load_view($view);
	}
	
	load_view('backend/layout/footer');
}

function delete_file($file_path) {
	if( is_file($file_path) ) {
		unlink($file_path);
	}
}

function format_datetime($datetime) {
	return date('j M, Y - h:ia', strtotime($datetime));
}

function format_date($date) {
	return date('j M, Y', strtotime($date));
}

function format_time($time) {
	return date('h:i A', strtotime($time));
}

function timezone_datetime($datetime = '') {
	$timezone_datetime = new DateTime($datetime, new DateTimeZone('Asia/Kolkata'));
	return $timezone_datetime;
}

function posted_ago($datetime, $full = false) {
	$now = timezone_datetime();
    $ago = timezone_datetime($datetime);

    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}


function debug($item = array(), $die = true, $display = true) {
	if( is_array($item) || is_object($item) ) {
		echo "<pre ".($display?'':'style="display:none"').">"; print_r($item); echo "</pre>";
	} else {
		echo $item;
	}
	
	if( $die ) {
		die();
	}
}

function ci_debug() {
	$CI =& get_instance();
	$CI->output->enable_profiler(TRUE);
}


function random_code($length = 16) {
	$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    $code = substr( str_shuffle( $chars ), 0, $length );
	return $code;
}

function set_flashdata($name, $message, $class='') {
	$CI =& get_instance();
	$data = array(
		'message' => '<div class="'.TOGGLE_CLOSE_CLASS.' alert alert-'.$class.'">'.$message.'</div>',
		'type' => $class
	);
	$CI->session->set_flashdata($name, $data);
}

function get_flashdata($name) {
	$CI =& get_instance();
	$data = $CI->session->flashdata($name);
	return $data['message'];
}

function set_notification($message, $class) {
	set_flashdata('notification', $message, $class);
}

function get_notification() {
	$data = get_flashdata('notification');
	return $data;
}

function set_login_sessions($user) {
	$userdata = get_row('userdata', $user['id'], 'user_id');
	$data = array(
		'logged_in' => 1,
		'user_id' => $user['id'],
		'user_type' => $user['user_type'],
		'username' => $user['username'],
		'fullname' => $userdata['firstname'].' '.$userdata['lastname']
	);
	set_sessions($data);
}

function delete_document($file) {
	$document_path = FCPATH.'data/documents/'.$file;
	if( is_file($document_path) ) unlink($document_path);
}


function truncate($string, $word_count = 10) {
 	$string = htmlspecialchars_decode(strip_tags($string));
    $words = explode(' ', $string);

    $output = '';
    foreach( $words as $key=>$word ) {
    	if( $key < $word_count ) {
    		$output .= $word.' ';
    	}
    }
    if( sizeof( $words ) > $word_count ) {
    	return $output.'...';
	}
	return $output;
}

function profile_image($file) {
	$profile_picture_path = FCPATH.'data/profile_picture/'.$file;
	if( is_file($profile_picture_path) ) {
		$file_path = base_url('data/profile_picture/$file');
	} else {
		$file_path = base_url('assets/img/default_profile_picture.png');
	}
	return $file_path;
}

// =============================================
// =============================================
// ================= ADMIN PANEL ===============
// =============================================
// =============================================

function set_admin_sessions($admin) {
	$data = array(
		'admin_logged_in' => 'yes',
		'admin_firstname' => $admin['firstname'],
		'admin_lastname' => $admin['lastname'],
		'admin_fullname' => $admin['firstname'].' '.$admin['lastname']
	);
	set_sessions($data);
}

function unset_admin_sessions() {
	$data = array(
		'admin_logged_in',
		'admin_firstname',
		'admin_lastname',
		'admin_role'
	);
	foreach( $data as $value ) {
		unset_session($value);
	}
}

function admin_access() {
	if( get_session('admin_frontend_login') == 1 ) {
		unset_login_session();

		$data['admin_logged_in'] = '1';
		set_sessions($data);
	}
}

function check_admin_logged_in() {
	if( !admin_logged_in() ) {
		redirect('admin');
	}
}

function admin_logged_in() {
	if( get_session('admin_logged_in') == "yes" ) {
		return true;
	}
	return false;
}

function load_admin_page($data){
	$CI =& get_instance();
	load_view('admin/base/base',$data);
}

function json_output($json) {
    $CI = & get_instance();
    $CI->output->set_content_type('application/json');
    $CI->output->set_output(json_encode($json));
}