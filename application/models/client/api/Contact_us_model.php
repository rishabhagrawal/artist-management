<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	function saveContactUs() {
		$add_data = json_decode(trim(file_get_contents('php://input')), true);
		if( is_null($add_data) ) {
			$this->output->set_status_header(400);
			$data['status'] = "fail";
			$data['error_msg'] = "invalid json";
		} else {
			if( isset($add_data['name']) && isset($add_data['contact_no']) && isset($add_data['subject']) && isset($add_data['email']) &&  !empty($add_data['name']) && !empty($add_data['contact_no']) && !empty($add_data['email']) && !empty($add_data['subject']) ) {
				$insert_data = array(
					'name' => $add_data['name'],
					'mobile_number' => $add_data['contact_no'],
					'email' => $add_data['email'],
					'subject' => isset($add_data['subject']) ? $add_data['subject'] : '',
					'message' => isset($add_data['message']) ? $add_data['message'] : ''
				);
				$query = $this->db->insert('contact_us', $insert_data);
				if ($query) {
					$this->output->set_status_header(200);
					$data['status'] = "success";
				} else {
					$this->output->set_status_header(500);
					$data['status'] = "fail";
					$data['error_msg'] = "data not inserted";
				}
			} else {
				$this->output->set_status_header(400);
				$data['status'] = "fail";
				$data['error_msg'] = "invalid data";
			}
		}
		return $data;
	}

}