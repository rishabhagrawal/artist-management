<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Artist_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	function featured() {
		$this->db->select("artists.*");
		$this->db->select("GROUP_CONCAT(category.name SEPARATOR ', ') as category", false);
		$this->db->select("location.name as location");
		$this->db->select("artist_images.image");
		$this->db->join('category', 'FIND_IN_SET(category.id,artists.category_id)', 'left');
		$this->db->join('location', 'location.id = artists.location_id', 'left');
		$this->db->join('artist_images', 'artist_images.artist_id = artists.id', 'left');
		$this->db->where('artists.is_featured', 1);
		$this->db->group_by('artists.id');
		$query = $this->db->get('artists');
		$this->output->set_status_header(200);
		$data['status'] = "success";
		$data['artists'] = array();
		if( $query->num_rows() > 0 ) {
			foreach( $query->result_array() as $key => $artist_data ) {
				$artist = array(
					'id' => intval($artist_data['id']),
					'name' => $artist_data['name'],
					'url' => getFriendlyURL($artist_data['name']),
					'website_link' => $artist_data['website_link'],
					'location' => $artist_data['location'],
					'category' => $artist_data['category'],
					'facebook_link' => $artist_data['facebook_link'],
					'twitter_link' => $artist_data['twitter_link'],
					'instagram_link' => $artist_data['instagram_link'],
					'video_link' => $artist_data['video_link'],
					'bio' => $artist_data['bio'],
					'image' => !empty($artist_data['image'])?base_url('data/artists/'.$artist_data['image']):''
				);
				array_push($data['artists'], $artist);
			}
		}
		return $data;
	}

	function all_list() {
		if( isset($_GET['appliedFilter']) ) {
			$feature_data = json_decode($_GET['appliedFilter'], TRUE);
			$where = array(
				'artists.is_delete' => 0
			);

			$page = (isset($feature_data['page']) && !empty($feature_data['page'])) ? $feature_data['page'] : 1; 
			$count = (isset($feature_data['count']) && !empty($feature_data['count'])) ? $feature_data['count'] : 20;
			if( isset($feature_data['name']) ) $like['artists.name'] = $feature_data['name'];
			if( isset($feature_data['email']) ) $like['artists.email'] = $feature_data['email'];
			if( isset($feature_data['location']) ) $where['artists.location_id'] = $feature_data['location'];

			$number = ($page - 1) * $count;
			$next_page = intval($page+1);

			$this->db->select('count(artists.id) as total_count');
			if( isset($like) && count($like) > 0  ) {
				$this->db->like($like);
			}
			$this->db->where($where);
			if( isset($feature_data['category']) ) {
				$this->db->where('find_in_set('.$feature_data['category'].', artists.category_id)');
			}
			$total_data = $this->db->get('artists');
			$total_data_row = $total_data->row_array();
			$data['total'] = intval($total_data_row['total_count']);

			if( $next_page*$count <= $data['total'] ) {
				$data['next_page'] = intval($next_page);
			} else {
				$data['next_page'] = null;
			}

			$data['artists'] = array();
			$this->db->select("artists.*");
			$this->db->select("GROUP_CONCAT(category.name SEPARATOR ', ') as category", false);
			$this->db->select("location.name as location");
			$this->db->select("artist_images.image");
			$this->db->join('category', 'FIND_IN_SET(category.id,artists.category_id)', 'left');
			$this->db->join('location', 'location.id = artists.location_id', 'left');
			$this->db->join('artist_images', 'artists.id = artist_images.artist_id', 'left');
			$this->db->where($where);
			if( isset($feature_data['category']) ) {
				$this->db->where('find_in_set('.$feature_data['category'].', artists.category_id)');
			}
			$this->db->group_by('artists.id');
			$this->db->limit($count, $number);
			if( isset($like) && count($like) > 0  ) {
				$this->db->like($like);
			}
			$query = $this->db->get('artists');
			if( $query->num_rows() > 0 ) {
				foreach( $query->result_array() as $key => $artist_data ) {
					$artist = array(
						'id' => intval($artist_data['id']),
						'name' => $artist_data['name'],
						'url' => getFriendlyURL($artist_data['name']),
						'website_link' => $artist_data['website_link'],
						'location' => $artist_data['location'],
						'category' => $artist_data['category'],
						'facebook_link' => $artist_data['facebook_link'],
						'twitter_link' => $artist_data['twitter_link'],
						'instagram_link' => $artist_data['instagram_link'],
						'video_link' => $artist_data['video_link'],
						'bio' => $artist_data['bio'],
						'image' => !empty($artist_data['image'])?base_url('data/artists/'.$artist_data['image']):''
					);
					array_push($data['artists'], $artist);
				}
			}
		} else {
			$this->output->set_status_header(400);
			$data['status'] = "fail";
			$data['error_msg'] = "invalid artist id";
		}
		return $data;
	}

	function detail($artist_id) {
		if( !empty($artist_id) ) {
			$this->db->select("artists.*");
			$this->db->select("GROUP_CONCAT(category.name SEPARATOR ', ') as category", false);
			$this->db->select("location.name as location");
			$this->db->select("artist_images.image");
			$this->db->join('category', 'FIND_IN_SET(category.id,artists.category_id)', 'left');
			$this->db->join('location', 'location.id = artists.location_id', 'left');
			$this->db->join('artist_images', 'artist_images.artist_id = artists.id', 'left');
			$this->db->group_by('artists.id');
			$this->db->where('artists.id',$artist_id);
			$query = $this->db->get('artists');
			if( $query->num_rows() > 0 ) {
				$artist_data = $query->row_array();
				$data['artist_data'] = array(
					'id' => intval($artist_data['id']),
					'name' => $artist_data['name'],
					'image' => !empty($artist_data['image'])?base_url('data/artists/'.$artist_data['image']):'',
					'website_link' => $artist_data['website_link'],
					'location' => $artist_data['location'],
					'category' => $artist_data['category'],
					'facebook_link' => $artist_data['facebook_link'],
					'twitter_link' => $artist_data['twitter_link'],
					'instagram_link' => $artist_data['instagram_link'],
					'video_link' => $artist_data['video_link'],
					'bio' => $artist_data['bio'],
					'is_featured' => intval($artist_data['is_featured']),
				);
				$this->output->set_status_header(200);
				$data['status'] = "success";
			} else {
				$this->output->set_status_header(400);
				$data['status'] = "fail";
				$data['error_msg'] = "invalid data";
			}
		} else {
			$this->output->set_status_header(400);
			$data['status'] = "fail";
			$data['error_msg'] = "invalid data";
		}
		
		return $data;
	}

}