<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Register_request_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	function saveRegisterRequest() {
		$add_data = json_decode(trim(file_get_contents('php://input')), true);
		if( is_null($add_data) ) {
			$this->output->set_status_header(400);
			$data['status'] = "fail";
			$data['error_msg'] = "invalid json";
		} else {
			if( isset($add_data['name']) && isset($add_data['category']) && isset($add_data['location']) && isset($add_data['bio']) && isset($add_data['email']) &&  !empty($add_data['name']) && !empty($add_data['category']) && !empty($add_data['location']) && !empty($add_data['bio']) && !empty($add_data['email']) ) {
				$insert_data = array(
					'name' => $add_data['name'],
					'category_id' => $add_data['category'],
					'location_id' => $add_data['location'],
					'bio' => $add_data['bio'],
					'links' => isset($add_data['website']) ? $add_data['website'] : '',
					'mobile_number' => isset($add_data['contact_no']) ? $add_data['contact_no'] : '',
					'email' => isset($add_data['email']) ? $add_data['email'] : ''
				);
				$query = $this->db->insert('register_request', $insert_data);
				if ($query) {
					$this->output->set_status_header(200);
					$data['status'] = "success";
				} else {
					$this->output->set_status_header(500);
					$data['status'] = "fail";
					$data['error_msg'] = "data not inserted";
				}
			} else {
				$this->output->set_status_header(400);
				$data['status'] = "fail";
				$data['error_msg'] = "invalid data";
			}
		}
		return $data;
	}
}