<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Post_requirement_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	function savePostRequirement() {
		$add_data = json_decode(trim(file_get_contents('php://input')), true);
		if( is_null($add_data) ) {
			$this->output->set_status_header(400);
			$data['status'] = "fail";
			$data['error_msg'] = "invalid json";
		} else {
			if( isset($add_data['name']) && isset($add_data['category']) && isset($add_data['location'])  && isset($add_data['contact_no']) && isset($add_data['email']) && isset($add_data['event_type']) && isset($add_data['event_date']) && isset($add_data['no_of_guests']) &&  !empty($add_data['name']) && !empty($add_data['category']) && !empty($add_data['location']) && !empty($add_data['contact_no']) && !empty($add_data['email']) && !empty($add_data['event_type']) && !empty($add_data['event_date']) && !empty($add_data['no_of_guests']) ) {
				$insert_data = array(
					'name' => $add_data['name'],
					'category_id' => $add_data['category'],
					'location_id' => $add_data['location'],
					'event_type' => $add_data['event_type'],
					'event_date' => $add_data['event_date'],
					'no_of_guests' => $add_data['no_of_guests'],
					'email' => $add_data['email'],
					'mobile_number' => $add_data['contact_no'],
					'message' => isset($add_data['message']) ? $add_data['message'] : ''
				);
				$query = $this->db->insert('posted_requirement', $insert_data);
				if ($query) {
					$this->output->set_status_header(200);
					$data['status'] = "success";
				} else {
					$this->output->set_status_header(500);
					$data['status'] = "fail";
					$data['error_msg'] = "data not inserted";
				}
			} else {
				$this->output->set_status_header(400);
				$data['status'] = "fail";
				$data['error_msg'] = "invalid data";
			}
		}
		return $data;
	}

}