<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Constant_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	function locations() {
		$this->output->set_status_header(200);
		$data['status'] = "success";
		$data['locations'] = array();
		$this->db->where('status', 1);
		$this->db->order_by('name', 'asc');
		$query = $this->db->get('location');
		if( $query->num_rows() > 0 ) {
			foreach( $query->result_array() as $key => $value ) {
				$location['id'] = intval($value['id']);
				$location['name'] = $value['name'];
				array_push($data['locations'], $location);
			}
		}
		return $data;
	}

	function categories() {
		$this->output->set_status_header(200);
		$data['status'] = "success";
		$data['categories'] = array();
		$this->db->where('status', 1);
		$this->db->order_by('name', 'asc');
		$query = $this->db->get('category');
		if( $query->num_rows() > 0 ) {
			foreach( $query->result_array() as $key => $value ) {
				$category['id'] = intval($value['id']);
				$category['name'] = $value['name'];
				array_push($data['categories'], $category);
			}
		}
		return $data;
	}

	function artistsCategories() {
		$this->output->set_status_header(200);
		$data['status'] = "success";
		$artistList = array();
		$categoryList = array();

		$this->db->where('status', 1);
		$this->db->order_by('name', 'asc');
		$query = $this->db->get('category');
		if( $query->num_rows() > 0 ) {
			foreach( $query->result_array() as $key => $value ) {
				$categoryObj = array();
				$categoryObj['id'] = intval($value['id']);
				$categoryObj['name'] = $value['name'];
				$categoryObj['url'] = getFriendlyURL($value['name']);
				$categoryObj['type'] = 'category';
				array_push($categoryList, $categoryObj);
			}
		}

		$this->db->where('status', 1);
		$this->db->order_by('name', 'asc');
		$query = $this->db->get('artists');
		if( $query->num_rows() > 0 ) {
			foreach( $query->result_array() as $key => $value ) {
				$categoryObj = array();
				$categoryObj['id'] = intval($value['id']);
				$categoryObj['url'] = getFriendlyURL($value['name']);
				$categoryObj['name'] = $value['name'];
				$categoryObj['type'] = 'artist';
				array_push($categoryList, $categoryObj);
			}
		}

		$data['result'] = array_merge($artistList,$categoryList);
		return $data;
	}
	


}