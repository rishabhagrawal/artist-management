<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Artist_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	function all_list() {
		if( isset($_GET['appliedFilter']) ) {
			$feature_data = json_decode($_GET['appliedFilter'], TRUE);
			$where = array(
				'artists.is_delete' => 0
			);

			$page = (isset($feature_data['page']) && !empty($feature_data['page'])) ? $feature_data['page'] : 1; 
			$count = (isset($feature_data['count']) && !empty($feature_data['count'])) ? $feature_data['count'] : 20;
			if( isset($feature_data['name']) ) $like['artists.name'] = $feature_data['name'];
			if( isset($feature_data['email']) ) $like['artists.email'] = $feature_data['email'];
			if( isset($feature_data['location']) ) $where['artists.location_id'] = $feature_data['location'];

			$number = ($page - 1) * $count;
			$next_page = intval($page+1);

			$this->db->select('count(artists.id) as total_count');
			if( isset($like) && count($like) > 0  ) {
				$this->db->like($like);
			}
			$this->db->where($where);
			if( isset($feature_data['category']) ) {
				$this->db->where('find_in_set('.$feature_data['category'].', artists.category_id)');
			}
			$total_data = $this->db->get('artists');
			$total_data_row = $total_data->row_array();
			$data['total'] = intval($total_data_row['total_count']);

			if( $next_page*$count <= $data['total'] ) {
				$data['next_page'] = intval($next_page);
			} else {
				$data['next_page'] = null;
			}

			$data['artists'] = array();
			$this->db->select("artists.*");
			$this->db->select("GROUP_CONCAT(category.name SEPARATOR ', ') as category", false);
			$this->db->select("location.name as location");
			$this->db->join('category', 'FIND_IN_SET(category.id,artists.category_id)', 'left');
			$this->db->join('location', 'location.id = artists.location_id', 'left');
			$this->db->where($where);
			if( isset($feature_data['category']) ) {
				$this->db->where('find_in_set('.$feature_data['category'].', artists.category_id)');
			}
			$this->db->group_by('artists.id');
			$this->db->limit($count, $number);
			if( isset($like) && count($like) > 0  ) {
				$this->db->like($like);
			}
			$query = $this->db->get('artists');
			// debug($this->db->last_query());
			if( $query->num_rows() > 0 ) {
				foreach( $query->result_array() as $key => $value ) {
					$artist['id'] = intval($value['id']);
					$artist['name'] = $value['name'];
					$artist['location'] = $value['location'];
					$artist['category'] = $value['category'];
					$artist['live_date'] = date('dS, M Y', strtotime($value['live_from']));
					$artist['live_time'] = date('h:i a', strtotime($value['live_from']));
					$artist['is_pause'] = intval($value['is_pause']);
					array_push($data['artists'], $artist);
				}
			}
		} else {
			$this->output->set_status_header(400);
			$data['status'] = "fail";
			$data['error_msg'] = "invalid artist id";
		}
		return $data;
	}

	function pause() {
		if( isset($_GET['artist_id']) && !empty($_GET['artist_id']) ) {
			$artist_id = $_GET['artist_id'];
			$artist = get_row('artists', $artist_id);
			if( $artist ) {
				$value = ($artist['is_pause'] == 1) ? 0 : 1;
				set_value('is_pause', $value, 'artists', $artist_id);
				$this->output->set_status_header(200);
				$data['status'] = "success";
			} else {
				$this->output->set_status_header(400);
				$data['status'] = "fail";
				$data['error_msg'] = "invalid artist id";
			}
		} else {
			$this->output->set_status_header(400);
			$data['status'] = "fail";
			$data['error_msg'] = "empty artist id";
		}
		return $data;
	}

	function delete() {
		if( isset($_GET['artist_id']) && !empty($_GET['artist_id']) ) {
			$artist_id = $_GET['artist_id'];
			$artist = get_row('artists', $artist_id);
			if( $artist ) {
				set_value('is_delete', 1, 'artists', $artist_id);
				$this->output->set_status_header(200);
				$data['status'] = "success";
			} else {
				$this->output->set_status_header(400);
				$data['status'] = "fail";
				$data['error_msg'] = "invalid artist id";
			}
		} else {
			$this->output->set_status_header(400);
			$data['status'] = "fail";
			$data['error_msg'] = "empty artist id";
		}
		return $data;
	}

	function add_artist() {
		$add_data = json_decode(trim(file_get_contents('php://input')), true);
		if( is_null($add_data) ) {
			$this->output->set_status_header(400);
			$data['status'] = "fail";
			$data['error_msg'] = "invalid json";
		} else {
			if( isset($add_data['name']) && isset($add_data['location_id']) && isset($add_data['category_id']) &&  !empty($add_data['name']) && !empty($add_data['location_id'])  && !empty($add_data['category_id']) ) {
				$insert_data = array(
					'name' => $add_data['name'],
					'email' => $add_data['email'],
					'website_link' => $add_data['website_link'],
					'location_id' => $add_data['location_id'],
					'category_id' => $add_data['category_id'],
					'landline_number' => $add_data['landline_number'],
					'facebook_link' => isset($add_data['facebook_link']) ? $add_data['facebook_link'] : '',
					'twitter_link' => isset($add_data['twitter_link']) ? $add_data['twitter_link'] : '',
					'instagram_link' => isset($add_data['instagram_link']) ? $add_data['instagram_link'] : '',
					'contact_person_name' => $add_data['contact_person_name'],
					'contact_person_email' => $add_data['contact_person_email'],
					'contact_person_mobile' => $add_data['contact_person_mobile'],
					'video_link' => $add_data['video_link'],
					'bio' => isset($add_data['bio']) ? $add_data['bio'] : '',
					'is_featured' => $add_data['is_featured'],
				);
				$query = $this->db->insert('artists', $insert_data);
				if ($query) {
					$this->output->set_status_header(200);
					$data['status'] = "success";
					$artist_id = $this->db->insert_id();
					$data['artist_id'] = intval($artist_id);
				} else {
					$this->output->set_status_header(500);
					$data['status'] = "fail";
					$data['error_msg'] = "data not inserted";
				}
			} else {
				$this->output->set_status_header(400);
				$data['status'] = "fail";
				$data['error_msg'] = "invalid data";
			}
		}
		return $data;
	}

	function edit_artist() {
		$add_data = json_decode(trim(file_get_contents('php://input')), true);
		if( is_null($add_data) ) {
			$this->output->set_status_header(400);
			$data['status'] = "fail";
			$data['error_msg'] = "invalid json";
		} else {
			if( isset($add_data['id']) && isset($add_data['name']) && isset($add_data['location_id']) && isset($add_data['category_id']) &&  !empty($add_data['id']) && !empty($add_data['name']) && !empty($add_data['location_id']) && !empty($add_data['category_id']) ) {
				$insert_data = array(
					'name' => $add_data['name'],
					'email' => $add_data['email'],
					'website_link' => $add_data['website_link'],
					'location_id' => $add_data['location_id'],
					'category_id' => $add_data['category_id'],
					'landline_number' => $add_data['landline_number'],
					'facebook_link' => isset($add_data['facebook_link']) ? $add_data['facebook_link'] : '',
					'twitter_link' => isset($add_data['twitter_link']) ? $add_data['twitter_link'] : '',
					'instagram_link' => isset($add_data['instagram_link']) ? $add_data['instagram_link'] : '',
					'contact_person_name' => $add_data['contact_person_name'],
					'contact_person_email' => $add_data['contact_person_email'],
					'contact_person_mobile' => $add_data['contact_person_mobile'],
					'video_link' => $add_data['video_link'],
					'bio' => isset($add_data['bio']) ? $add_data['bio'] : '',
					'is_featured' => $add_data['is_featured'],
				);
				$this->db->where('id', $add_data['id']);
				$query = $this->db->update('artists', $insert_data);
				if ($query) {
					$this->output->set_status_header(200);
					$data['status'] = "success";
				} else {
					$this->output->set_status_header(500);
					$data['status'] = "fail";
					$data['error_msg'] = "data not inserted";
				}
			} else {
				$this->output->set_status_header(400);
				$data['status'] = "fail";
				$data['error_msg'] = "invalid data";
			}
		}
		return $data;
	}

	function images() {
		if( isset($_FILES['image']['name']) && isset($_POST['artist_id']) ) {
            if( !empty($_POST['artist_id']) && !empty($_FILES['image']['name']) ) {
                $ext = get_extention($_FILES['image']);
                $artist_id = $_POST['artist_id'];

                $file_name = time().rand(10000, 99999).'-'.$artist_id.'.'.$ext;
                $source_path = $_FILES['image']['tmp_name'];
                $upload_path = FCPATH.'/data/artists/'.$file_name;

                $this->load->library('image_lib');
                $config = array(
                    'image_library' => 'gd2',
                    'source_image' => $source_path,
                    'new_image' => $upload_path,
                    'maintain_ratio' => true,
                    'width' => 1024
                );
                $this->image_lib->initialize($config);
                if( $this->image_lib->resize() ) {
	                $insert_data = array(
	                    'artist_id' => $artist_id,
	                    'image' => $file_name,
	                );
	                $query = $this->db->insert('artist_images', $insert_data);
	                if( $query ) {
	                    $this->output->set_status_header(200);
	                    $data['status'] = "success";
	                } else {
	                    $this->output->set_status_header(400);
	                    $data['status'] = "fail";
	                    $data['error_msg'] = "query fail";
	                }
                }else{
                	$this->output->set_status_header(400);
                    $data['status'] = "fail";
                    $data['error_msg'] = "resize fail";
                }
            } else {
                $this->output->set_status_header(400);
                $data['status'] = "fail";
                $data['error_msg'] = "invalid data";
            }
        } else {
            $this->output->set_status_header(400);
            $data['status'] = "fail";
            $data['error_msg'] = "invalid data";
        }
        return $data;
	}


	function get_artist_detail($artist_id) {
		if( !empty($artist_id) ) {
			$this->db->select('artists.*, artist_images.image');
			$this->db->join('artist_images', 'artists.id = artist_images.artist_id', 'left');
			$this->db->where('artists.id', $artist_id);
			$query = $this->db->get('artists');
			if( $query->num_rows() > 0 ) {
				$artist_data = $query->row_array();
				$data['artist_data'] = array(
					'id' => intval($artist_data['id']),
					'name' => $artist_data['name'],
					'email' => $artist_data['email'],
					'image' => $artist_data['image'],
					'website_link' => $artist_data['website_link'],
					'location_id' => $artist_data['location_id'],
					'category_id' => $artist_data['category_id'],
					'landline_number' => $artist_data['landline_number'],
					'facebook_link' => $artist_data['facebook_link'],
					'twitter_link' => $artist_data['twitter_link'],
					'instagram_link' => $artist_data['instagram_link'],
					'contact_person_name' => $artist_data['contact_person_name'],
					'contact_person_email' => $artist_data['contact_person_email'],
					'contact_person_mobile' => $artist_data['contact_person_mobile'],
					'video_link' => $artist_data['video_link'],
					'bio' => $artist_data['bio'],
					'is_featured' => $artist_data['is_featured'],
				);
				$this->output->set_status_header(200);
				$data['status'] = "success";
			} else {
				$this->output->set_status_header(400);
				$data['status'] = "fail";
				$data['error_msg'] = "invalid data";
			}
		} else {
			$this->output->set_status_header(400);
			$data['status'] = "fail";
			$data['error_msg'] = "invalid data";
		}
		
		return $data;
	}

	function delelte_image($artist_id) {
		if( !empty($artist_id) ) {
			$image_name = get_value('image', 'artist_images', $artist_id, 'artist_id');
			if( $image_name ) {
				$this->db->where('artist_id', $artist_id);
				$this->db->delete('artist_images');

				$file_name = FCPATH.'/data/artists/'.$image_name;
				delete_file($file_name);
				
				$this->output->set_status_header(200);
				$data['status'] = "success";
			} else {
				$this->output->set_status_header(400);
				$data['status'] = "fail";
				$data['error_msg'] = "invalid artist id";
			}
		} else {
			$this->output->set_status_header(400);
			$data['status'] = "fail";
			$data['error_msg'] = "invalid data";
		}
	}

}