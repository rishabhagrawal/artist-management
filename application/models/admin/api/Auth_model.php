<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->helper('cookie');
	}

	function index() {

	}

	function login() {
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$login_data = json_decode(trim(file_get_contents('php://input')), true);
			if( is_null($login_data) ) {
				$this->output->set_status_header(400);
				$data['status'] = "fail";
				$data['error_msg'] = "invalid json";
			} else {
				if( isset($login_data['email_id']) && isset($login_data['password']) && !empty($login_data['email_id']) && !empty($login_data['password']) ) {
						
					$email = trim($login_data['email_id']);
					$password = md5($login_data['password']);
					
					$this->db->where('email_id', $email);
					$this->db->where('status', '1');
					$query = $this->db->get('admin');

					if( $query->num_rows() > 0 ) {
						$row = $query->row_array();
						if( $password == $row['password'] ) {

							//set server cookie							
							$cookie_data = array(
								'name'   => 'admin_token_id',
								'value'  => $row['token_id'],
								'expire' => '886500',
								'path'   => '/',
								'httponly' => true
							);

							$this->input->set_cookie($cookie_data);

							//set session..
							$user_data = array(
								'id' => $row['id'],
								'name' => $row['name'],
								'email_id' => $row['email_id'],
								'admin_logged_in' => 1
							);

							set_sessions($user_data);
							$data['status'] = "Login Successful";
							$this->output->set_status_header(200);
							
						} else {
							$this->output->set_status_header(200);
							$data['status'] = "fail";
							$data['error_msg'] = "invalid email id or password!";	
						}
					} else {
						$this->output->set_status_header(200);
						$data['status'] = "fail";
						$data['error_msg'] = "invalid email id or password!";
					}
				} else {
					$this->output->set_status_header(400);
					$data['status'] = "fail";
					$data['error_msg'] = "empty data";
				}
			}	
		} else{
			$this->output->set_status_header(405);
			$data['status'] = "fail";
			$data['error_msg'] = "405 Method Not Allowed";
		}
		return $data;
	}


	function logout() {
		delete_cookie("admin_token_id");
		$this->session->sess_destroy();
		$data['status'] = 'success';		
		$data['message'] = 'successfully logout';		
		return $data;
	}


}