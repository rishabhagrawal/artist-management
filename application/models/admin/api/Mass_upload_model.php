<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mass_upload_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	function upload_file() {
		if( isset($_FILES['file']['name']) && !empty($_FILES['file']['name']) ) {
			$ext = get_extention($_FILES['file']);
			if( $ext == "csv" || $ext == "xls" || $ext == "xlsx" ) {
				$inputFileName = $_FILES['file']['tmp_name'];

				include FCPATH.'assets/php-excel/PHPExcel/IOFactory.php';
				// echo 'Loading file ',pathinfo($inputFileName,PATHINFO_BASENAME),' using IOFactory to identify the format<br />';
				try {
					$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
				} catch(Exception $e){
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
				}

				$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
				$number_of_rows = count($sheetData);
				if( $number_of_rows > 0 ) {
					for($a = 2; $a <= $number_of_rows; $a++) {
						if( !empty($sheetData[$a]['B']) ) {
							$insert_data = array();

							$image = trim($sheetData[$a]['B']);
							$insert_data['name'] = $sheetData[$a]['C'];
							$insert_data['bio'] = $sheetData[$a]['D'];
							// $insert_data['video_link'] = $sheetData[$a]['C'];
							$genre_type = $sheetData[$a]['E'];
							$location = trim($sheetData[$a]['F']);
							// $insert_data['facebook_link'] = $sheetData[$a]['G'];
							// $insert_data['twitter_link'] = $sheetData[$a]['H'];
							// $insert_data['instagram_link'] = $sheetData[$a]['I'];
							// $insert_data['website_link'] = $sheetData[$a]['J'];
							// $insert_data['contact_person_name'] = $sheetData[$a]['K'];
							// $insert_data['contact_person_mobile'] = $sheetData[$a]['L'];
							// $insert_data['contact_person_email'] = $sheetData[$a]['M'];
							// $insert_data['landline_number'] = $sheetData[$a]['N'];
							// $insert_data['email'] = $sheetData[$a]['O'];
							// $insert_data['tags'] = $sheetData[$a]['P'];

							$insert_data['location_id'] = 0;
							$location_id = get_value('id', 'location', $location, 'name');
							if( $location_id ) {
								$insert_data['location_id'] = $location_id;
							} else {
								$location_data = array(
									'name' => $location
								);
								$query = $this->db->insert('location', $location_data);
								$insert_data['location_id'] = $this->db->insert_id();
							}

							$genre_type = array_map('trim', explode('/', $genre_type));
							
							$this->db->select('GROUP_CONCAT(id) as category_id');
							$this->db->where_in('name', $genre_type);
							$query = $this->db->get('category');
							if( $query->num_rows() > 0 ) {
								$row = $query->row_array();
								$insert_data['category_id'] = $row['category_id'];
							}
							$this->db->insert('artists', $insert_data);
							$artist_id = $this->db->insert_id();

							if( !empty($image) ) {
								$image_name = FCPATH.'data/artists/'.$image;
								if( file_exists($image_name) ) {
									$insert_data['image'] = $image;
									$image_data = array(
										'artist_id' => $artist_id,
										'image' => $image
									);
									$this->db->insert('artist_images', $image_data);
								}
							}

						} else {
							$this->output->set_status_header(400);
							$data['status'] = "fail";
							$data['error_msg'] = "Please check all fields";
						}
					}
					$this->output->set_status_header(200);
					$data['status'] = 'success';
					$data['msg'] = 'Upload Successfull';
				} else {
					$this->output->set_status_header(400);
					$data['status'] = "fail";
					$data['error_msg'] = "empty excel";
				}
			} else {
				$this->output->set_status_header(400);
				$data['status'] = "fail";
				$data['error_msg'] = "invalid file";
			}
		} else {
			$this->output->set_status_header(400);
			$data['status'] = "fail";
			$data['error_msg'] = "invalid data";
		}
		return $data;
	}

}
