<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Register_request_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	function all_list() {
		if( isset($_GET['appliedFilter']) ) {
			$feature_data = json_decode($_GET['appliedFilter'], TRUE);
			$where = array(
				'register_request.status' => 1
			);

			$page = (isset($feature_data['page']) && !empty($feature_data['page'])) ? $feature_data['page'] : 1; 
			$count = (isset($feature_data['count']) && !empty($feature_data['count'])) ? $feature_data['count'] : 20;
			if( isset($feature_data['name']) ) $like['register_request.name'] = $feature_data['name'];
			if( isset($feature_data['mobile_number']) ) $like['register_request.mobile_number'] = $feature_data['mobile_number'];
			if( isset($feature_data['location']) ) $where['register_request.location_id'] = $feature_data['location'];

			$number = ($page - 1) * $count;
			$next_page = intval($page+1);

			$this->db->select('count(register_request.id) as total_count');
			if( isset($like) && count($like) > 0  ) {
				$this->db->like($like);
			}
			$this->db->where($where);
			$total_data = $this->db->get('register_request');
			$total_data_row = $total_data->row_array();
			$data['total'] = intval($total_data_row['total_count']);

			if( $next_page*$count <= $data['total'] ) {
				$data['next_page'] = intval($next_page);
			} else {
				$data['next_page'] = null;
			}

			$data['results'] = array();
			$this->db->select("register_request.*, location.name as location");
			$this->db->join('location', 'location.id = register_request.location_id', 'left');
			$this->db->where($where);
			$this->db->limit($count, $number);
			if( isset($like) && count($like) > 0  ) {
				$this->db->like($like);
			}
			$query = $this->db->get('register_request');
			// debug($this->db->last_query());
			if( $query->num_rows() > 0 ) {
				foreach( $query->result_array() as $key => $value ) {
					$request['id'] = intval($value['id']);
					$request['name'] = $value['name'];
					$request['mobile_number'] = $value['mobile_number'];
					$request['location'] = $value['location'];
					$request['bio'] = $value['bio'];
					$request['created_date'] = date('dS, M Y', strtotime($value['created_at']));
					$request['created_time'] = date('h:i a', strtotime($value['created_at']));
					array_push($data['results'], $request);
				}
			}
		} else {
			$this->output->set_status_header(400);
			$data['status'] = "fail";
			$data['error_msg'] = "invalid data";
		}
		return $data;
	}
}