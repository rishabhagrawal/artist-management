<footer ng-controller="footerController"><div class="footer-container clearfix"><div class="left-wrap"><div class="logo-wrap"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/images/logo.png') ?>" alt="logo"><div class="logo-text">GET MY ARTIST</div></a></div><!-- <h5>HEAD OFFICE:</h5>
			<p>Lorem ipsum dolor sit amet, consectetur<br> adipisicing elit. Et </p> --><h5>PHONE:</h5><p>+91-8130080140</p><h5>EMAIL:</h5><p>gma@gmail.com</p><!-- <div class="social-icons">
				<ul class="clearfix">
					<li>
						<a href="#">
							<img src="<?php echo base_url('assets/images/facebook-logo.png') ?>" alt="facebook">
						</a>
					</li>
					<li>
						<a href="#">
							<img src="<?php echo base_url('assets/images/google-plus-logo.png') ?>" alt="google">
						</a>
					</li>
					<li>
						<a href="#">
							<img src="<?php echo base_url('assets/images/instagram-logo.png') ?>" alt="instagram">
						</a>
					</li>
				</ul>
			</div> --><div class="links"><ul><li><a href="<?php echo base_url() ?>">HOME</a></li><li><a href="<?php echo base_url('about-us') ?>">ABOUT US</a></li><li><a href="<?php echo base_url('listing') ?>">ARTISTS</a></li><li><a href="<?php echo base_url('requirement') ?>">REQUIREMENT</a></li><li><a href="<?php echo base_url('register') ?>">REGISTER</a></li></ul></div></div><div class="right-wrap"><div class="form-section"><h4>DROP US A MESSAGE</h4><ul class="clearfix"><li><div class="input-wrapper"><input type="text" placeholder="Name" ng-model="formData.name" ng-class="{'error' : nameError}"></div></li><li><div class="input-wrapper"><input type="text" placeholder="Email" ng-model="formData.email" ng-class="{'error' : emailError}"></div></li></ul><ul class="clearfix"><li><div class="input-wrapper"><input type="tel" placeholder="Phone Number" ng-model="formData.contact_no" ng-class="{'error' : contactError}" maxlength="10"></div></li><li><div class="input-wrapper"><input type="text" placeholder="Subject" ng-model="formData.subject"></div></li></ul><div class="input-wrapper"><textarea placeholder="Message" ng-model="formData.message"></textarea></div><div class="button-wrapper"><div class="success-msg" ng-show="successMsg">{{successMsg}}</div><button ng-click="submitBtn()">SEND NOW</button></div></div></div></div></footer>