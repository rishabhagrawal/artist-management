<?php 
// Name of Class as mentioned in $hook['post_controller]
class Record_log {

    function __construct() {
       // Anything except exit() :P
    }

    // Name of function same as mentioned in Hooks Config
    function record_param() {

        $CI = & get_instance();
        date_default_timezone_set('Asia/Kolkata');

        $url = $CI->config->site_url($CI->uri->uri_string());
        $current_url = $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;

        $filepath = APPPATH . 'logs/Record-Params-' . date('Y-m-d') . '.log'; // Creating Query Log file with today's date in application/logs folder
        $handle = fopen($filepath, "a+");           // Opening file with pointer at the end of the file

        $store_data = "\n\nExecute at : ".date('h:i:s A')."\n";

        $store_data .= "URL : ".$current_url ."\n";
        $store_data .= "Method : ".$CI->input->server('REQUEST_METHOD') ."\n\n";

        foreach ($_POST as $key => $value) {
            $store_data .= "'".$key."'" . "       :     '".$value."'\n"; 
        }

        foreach ($_GET as $key => $value) {
            $store_data .= "'".$key."'" . "       :     '".$value."'\n"; 
        }

        if(file_get_contents('php://input')){
            $json_data = json_decode(trim(file_get_contents('php://input')), true);
            $store_data .= json_encode($json_data);
        }

        $store_data .= "\n=========================================";
        fwrite($handle, $store_data . "\n");              // Writing it in the log file

        fclose($handle);      // Close the file
    }



}
